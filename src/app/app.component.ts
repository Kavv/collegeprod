import {Component, OnInit} from '@angular/core';
import {LoaderService} from "./services/shared/loader.service";
import {debounceTime, Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'college-front';
  loading$!: Observable<boolean>
  constructor(private loadingService: LoaderService) {
  }

  ngOnInit() {
    this.loading$ = this.loadingService.isLoading$.pipe(
      debounceTime(500)
    );
  }
}
