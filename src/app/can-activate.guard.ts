import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, flatMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { AuthService } from './services/auth/auth.service';

/**
 * Prevent access to routes if access-token is not present.
 *
 * @export
 * @class AuthGuard
 * @implements {CanActivate}
 */
@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private router: Router,
    private readonly location: Location,
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await this.authService.getLoggedtUser();
        const adminPaths = [
          '/users',
          '/cycles',
          '/groups',
          '/payments',
          '/accounting/accounts',
          '/accounting',
          '/report',
        ];
        
        const studentPath = [
          '/class-content',
          '/my-assignments',
          '/my-payments',
        ];

        const teacherPaths = [
          '/contents',
          '/assignments',
          '/teacher-assignments',
          '/assistances',
        ];

        const path = state.url;
        let invalid = true;

        // El rol super admin tiene acceso a todo
        if (user?.type_id === 5) {
          invalid = false;
          resolve(true);
        }
        
        // Si el usuario es estudiante y la url es parte de los modulos permitidos para el estudiante
        if (user?.type_id === 1 && studentPath.includes(path)) {
          invalid = false;
          resolve(true);
        }
        // Si el usuario es profesor y la url es parte de los modulos permitidos para el profesor
        if (user?.type_id === 2 && teacherPaths.includes(path)) {
          invalid = false;
          resolve(true);
        }

        // Si el usuario es administrativo y la url es parte de los modulos permitidos para el administrativo
        if (user?.type_id === 3 && adminPaths.includes(path)) {
          invalid = false;
          resolve(true);
        }

        if(invalid) {
          this.router.navigate(['/']);
          resolve(false);
        }


      } catch (e) {
        this.router.navigate(['/']);
        resolve(false);
      }
    });
  }
}
