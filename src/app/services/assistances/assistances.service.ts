import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AssistancesDto } from 'src/app/models/assistances/assistances-dto';
import { CreateAssistanceDto } from 'src/app/models/assistances/create-assistance-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AssistancesService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<AssistancesDto[]> {
    return this.http.get<AssistancesDto[]>(`${environment.baseURL}/assistances`);
  }

  create(body: CreateAssistanceDto): Observable<AssistancesDto> {
    return this.http.post<AssistancesDto>(`${environment.baseURL}/assistances`, body);
  }

  update(body: CreateAssistanceDto, id:number): Observable<AssistancesDto> {
    return this.http.put<AssistancesDto>(`${environment.baseURL}/assistances/${id}`, body);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${environment.baseURL}/assistances/${id}`);
  }

}
