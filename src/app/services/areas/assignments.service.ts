import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  constructor(private http: HttpClient) { }

  getAllAreas(): Observable<AreaDto[]> {
    return this.http.get<AreaDto[]>(`${environment.baseURL}/areas`);
  }
}

