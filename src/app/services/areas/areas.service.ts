import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<AreaDto[]> {
    return this.http.get<AreaDto[]>(`${environment.baseURL}/areas/list`);
  }

  create(body: {name: string}): Observable<AreaDto> {
    return this.http.post<AreaDto>(`${environment.baseURL}/areas/add`, body)
  }

  update(body: {name: string}, id: number): Observable<AreaDto> {
    return this.http.put<AreaDto>(`${environment.baseURL}/areas/edit/${id}`, body);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${environment.baseURL}/areas/delete/${id}`);
  }
}
