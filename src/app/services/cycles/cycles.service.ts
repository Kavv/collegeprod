import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CycleDto} from "../../models/cycles/cycle-dto";
import {environment} from "../../../environments/environment";
import {CreateCycleDto} from "../../models/cycles/create-cycle-dto";
import {CycleEditDto} from "../../models/cycles/cycle-edit-dto";
import {GroupDto} from "../../models/groups/group-dto";

@Injectable({
  providedIn: 'root'
})
export class CyclesService {

  constructor(private http: HttpClient) { }

  getAllCycles(): Observable<CycleDto[]> {
    return this.http.get<CycleDto[]>(`${environment.baseURL}/cycles/list`);
  }

  createCycle(body: CreateCycleDto): Observable<CycleDto> {
    return this.http.post<CycleDto>(`${environment.baseURL}/cycles/add`, body);
  }

  editCycle(id: number, body: CycleEditDto): Observable<CycleDto> {
    return this.http.put<CycleDto>(`${environment.baseURL}/cycles/edit/${id}`, body);
  }

  deleteCycle(id: number): Observable<any> {
    return this.http.delete(`${environment.baseURL}/cycles/delete/${id}`);
  }

  generateCycleStructure(id: number): Observable<GroupDto[]> {
    return this.http.post<GroupDto[]>(`${environment.baseURL}/cycles/structure`, {cycle_id: id});
  }

  getCycleStructure(id: number): Observable<GroupDto[]> {
    return this.http.post<GroupDto[]>(`${environment.baseURL}/cycles/structure/get`, { cycle_id: id });
  }
}
