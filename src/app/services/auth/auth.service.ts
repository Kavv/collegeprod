import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LoginDto} from "../../models/auth/login-dto";
import {environment} from "../../../environments/environment";
import {map, Observable, shareReplay} from "rxjs";
import {LoginResponseDto} from "../../models/auth/login-response-dto";
import * as moment from 'moment';
import { ProfileDto } from 'src/app/models/auth/profile-dto';
import { NewPasswordDto } from 'src/app/models/auth/new-password-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  _currentUser!: ProfileDto | undefined;
  private _currentUserPromise: Promise<ProfileDto|undefined> | null = null;

  login(body: LoginDto): Observable<LoginResponseDto> {
    return this.http.post<LoginResponseDto>(`${environment.baseURL}/auth/login`, body)
      .pipe(
        shareReplay(),
        map((response) => {
          this._currentUser = response.access_token.user;
          this.setSession(response);

          return response;
        })
      );
  }

  private setSession(authResult: LoginResponseDto) {
    const expiresAt = moment().add(authResult.expires, 'second');

    localStorage.setItem('token', authResult.access_token.token);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');

    this.http.post(`${environment.baseURL}/auth/logout`, {});
  }

  public isLoggedIn(): boolean {
    const expiration = this.getExpiration();

    if (expiration) {
      return moment().isBefore(expiration);
    }
    return false;
  }

  public isLoggedOut(): boolean {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    let expiresAt = null;
    if (expiration) {
      expiresAt = JSON.parse(expiration);
    }

    return expiresAt;

  }
  
  public getCurrentUser(): Observable<ProfileDto> {
    return this.http.get<ProfileDto>(`${environment.baseURL}/auth/me/`);
  }

  set currentUser(user: ProfileDto) {
    this._currentUser = user;
  }

  public async getLoggedtUser(): Promise<ProfileDto | undefined> {
      if (this._currentUser == null) {
        this._currentUserPromise = this.getCurrentUser().toPromise();
        this._currentUser = await this._currentUserPromise;
      }

      return this._currentUser;
  }

  changePassword(body: NewPasswordDto) {
    return this.http.post(`${environment.baseURL}/auth/changePassword/`, body);
  }


}
