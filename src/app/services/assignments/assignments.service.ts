import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { AssignmentResultDto } from 'src/app/models/assignments/assignment-result';

@Injectable({
  providedIn: 'root'
})
export class AssignmentsService {

  constructor(private http: HttpClient) { }

  getAllAssignments(params?: any): Observable<AssignmentDto[]> {
    return this.http.get<AssignmentDto[]>(`${environment.baseURL}/assignments`, {params});
  }

  store(body: AssignmentDto): Observable<AssignmentDto> {
    return this.http.post<AssignmentDto>(`${environment.baseURL}/assignments`, body);
  }

  update(id: number, body: AssignmentDto): Observable<AssignmentDto> {
    return this.http.put<AssignmentDto>(`${environment.baseURL}/assignments/${id}`, body);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${environment.baseURL}/assignments/${id}`);
  }


  myAssignments(): Observable<AssignmentDto[]> {
    return this.http.get<AssignmentDto[]>(`${environment.baseURL}/student/my-assignments`);
  }
  
  sendMyAssignment(body: AssignmentResultDto): Observable<AssignmentResultDto>  {
    return this.http.post<AssignmentResultDto>(`${environment.baseURL}/student/send-assignment`, body);
  }

  getStudentAssignment(id: number): Observable<AssignmentResultDto>  {
    return this.http.get<AssignmentResultDto>(`${environment.baseURL}/student/get-assignment/${id}`);
  }

  getAssignmentSent(id: number): Observable<AssignmentResultDto[]>  {
    return this.http.get<AssignmentResultDto[]>(`${environment.baseURL}/teachers/assignments-received/${id}`);
  }

  setResult(id: number, body: AssignmentResultDto): Observable<AssignmentResultDto> {
    return this.http.put<AssignmentResultDto>(`${environment.baseURL}/teachers/assignments-received/${id}`, body);
  }
}

