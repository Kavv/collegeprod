import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GroupDto} from "../../models/groups/group-dto";
import {environment} from "../../../environments/environment";
import {CreateGroupDto} from "../../models/groups/create-group-dto";
import {CycleEditDto} from "../../models/cycles/cycle-edit-dto";

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<GroupDto[]> {
    return this.http.get<GroupDto[]>(`${environment.baseURL}/groups/list`);
  }

  getByAllByCycle(id: number): Observable<GroupDto[]> {
    return this.http.get<GroupDto[]>(`${environment.baseURL}/groups/by-cycle/${id}`);
  }

  editGroup(id: number, body: CreateGroupDto): Observable<GroupDto> {
    return this.http.put<GroupDto>(`${environment.baseURL}/groups/edit/${id}`, body);
  }

  deleteGroup(id: number): Observable<any> {
    return this.http.delete(`${environment.baseURL}/groups/delete/${id}`);
  }

  createGroup(body: CreateGroupDto): Observable<GroupDto> {
    return this.http.post<GroupDto>(`${environment.baseURL}/groups/add`, body);
  }
}
