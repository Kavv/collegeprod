import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TeacherDto} from "../../models/teachers/teacher-dto";
import {environment} from "../../../environments/environment";
import {GroupDto} from "../../models/groups/group-dto";

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<TeacherDto[]> {
    return this.http.get<TeacherDto[]>(`${environment.baseURL}/teachers/list`);
  }

  assignTeacher(body: object): Observable<GroupDto> {
    return this.http.post<GroupDto>(`${environment.baseURL}/teachers/assign-group`, body);
  }

  unassignTeacher(body: {group_id: number, teacher_id: number}): Observable<any> {
    return this.http.post<any>(`${environment.baseURL}/teachers/unassign-group`, body);
  }
}


