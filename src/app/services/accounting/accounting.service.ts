import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {CreateAccount} from "../../models/accounting/create-account";
import {CreateMovement} from "../../models/accounting/create-movement";
import {Observable} from "rxjs";
import {AccountType} from "../../models/accounting/account-type";
import {Account} from "../../models/accounting/account";
import {Movement} from "../../models/accounting/movement";

@Injectable({
  providedIn: 'root'
})
export class AccountingService {

  constructor(private http: HttpClient) { }

  getAllAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(`${environment.baseURL}/accounting/accounts`);
  }

  createAccount(body: CreateAccount) {
    return this.http.post(`${environment.baseURL}/accounting/accounts`, body);
  }

  editAccount(id: number, body: CreateAccount) {
    return this.http.put(`${environment.baseURL}/accounting/accounts/${id}`, body);
  }

  deleteAccount(id: number) {
    return this.http.delete(`${environment.baseURL}/accounting/accounts/${id}`);
  }

  getAllAccountTypes(): Observable<AccountType[]> {
    return this.http.get<AccountType[]>(`${environment.baseURL}/accounting/account-types`);
  }

  createAccountType(body: {name: string}): Observable<AccountType> {
    return this.http.post<AccountType>(`${environment.baseURL}/accounting/account-types`, body);
  }


  editAccountType(id: number, body: { name: string }): Observable<AccountType> {
    return this.http.put<AccountType>(`${environment.baseURL}/accounting/account-types/${id}`, body);
  }

  deleteAccountType(id: number) {
    return this.http.delete(`${environment.baseURL}/accounting/account-types/${id}`);
  }

  getAllMovements(): Observable<Movement[]> {
    return this.http.get<Movement[]>(`${environment.baseURL}/accounting/movements`);
  }

  createMovement(body: CreateMovement) {
    return this.http.post(`${environment.baseURL}/accounting/movements`, body);
  }

  editMovement(id: number, body: CreateMovement) {
    return this.http.put(`${environment.baseURL}/accounting/movements/${id}`, body);
  }

  deleteMovement(id: number) {
    return this.http.delete(`${environment.baseURL}/accounting/movements/${id}`);
  }

  getAllMovementTypes() {
    return this.http.get(`${environment.baseURL}/accounting/movement-types`);
  }

  createMovementType(body: {name: string}) {
    return this.http.post(`${environment.baseURL}/accounting/movement-types`, body);
  }

  editMovementType(id: number, body: { name: string }) {
    return this.http.put(`${environment.baseURL}/accounting/movement-types/${id}`, body);
  }

  deleteMovementType(id: number) {
    return this.http.delete(`${environment.baseURL}/accounting/movement-types/${id}`);
  }
}
