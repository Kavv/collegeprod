import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContentDto } from 'src/app/models/contents/content-dto';
import { CreateContentDto } from 'src/app/models/contents/create-content-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContentsService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<ContentDto[]> {
    return this.http.get<ContentDto[]>(`${environment.baseURL}/contents/list`);
  }

  getStudentContentByClass(id: number): Observable<ContentDto[]> {
    return this.http.get<ContentDto[]>(`${environment.baseURL}/student/get-contents/${id}`);
  }

  create(body: CreateContentDto): Observable<ContentDto> {
    return this.http.post<ContentDto>(`${environment.baseURL}/contents/add`, body);
  }

  update(body: CreateContentDto, id: number): Observable<ContentDto> {
    return this.http.put<ContentDto>(`${environment.baseURL}/contents/edit/${id}`, body);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${environment.baseURL}/contents/delete/${id}`);
  }
}
