import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';
import { PaymentDto } from 'src/app/models/payments/payments';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(private http: HttpClient) { }
  
  base = environment.baseURL;
  public payments() {
    return this.http.get(`${this.base}/payments/`).pipe(catchError(this.errorHandler));
  }

  public store(data:any){
    return this.http.post(`${this.base}/payments`, data).pipe(catchError(this.errorHandler));
  }

  public update(data:any, id:number|null){
    return this.http.put(`${this.base}/payments/`+id, data).pipe(catchError(this.errorHandler));
  }

  public userPayments() {
    return this.http.get(`${this.base}/payments/userPayments/`).pipe(catchError(this.errorHandler));
  }

  public addStudentPayment(data:any){
    return this.http.post(`${this.base}/payments/add`, data).pipe(catchError(this.errorHandler));
  }

  public editUserPayments(data:any, id:number|null) {
    return this.http.put(`${this.base}/payments/userPayments/`+id, data).pipe(catchError(this.errorHandler));
  }

  public deleteUserPayment(id:number|null) {
    return this.http.delete(`${this.base}/payments/userPayments/`+id).pipe(catchError(this.errorHandler));
  }
  
  public paymentsByUser() {
    return this.http.get(`${this.base}/student/my-payments/`).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.status);
  }
  
}

