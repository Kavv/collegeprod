import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }
  
  base = environment.baseURL;
  public all(){
    return this.http.get(`${this.base}/users/`).pipe(catchError(this.errorHandler));
  }
  public students(type:number){
    return this.http.get(`${this.base}/users/list/`+type).pipe(catchError(this.errorHandler));
  }

  public getByType(type: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.base}/users/list/by-type/${type}`)
      .pipe(catchError(this.errorHandler));
  }

  public show(id:number){
    return this.http.get(`${this.base}/users/`+id).pipe(catchError(this.errorHandler));
  }
  
  public store(data:any){
    return this.http.post(`${this.base}/users`, data).pipe(catchError(this.errorHandler));
  }
  
  public update(data:any, id:number){
    return this.http.put(`${this.base}/users/`+id, data).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.status);
  }
  
  public delete(id:number){
      return this.http.delete(`${this.base}/users/`+id).pipe(catchError(this.errorHandler));
  }

  getTeacherAreas(): Observable<AreaDto[]>  {
    return this.http.get<AreaDto[]>(`${environment.baseURL}/users/current/areas/`);
  }

  getTeacherGroup(): Observable<GroupDto>  {
    return this.http.get<GroupDto>(`${environment.baseURL}/users/current/group/`);
  }
}

