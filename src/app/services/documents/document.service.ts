import { group } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(
    private http: HttpClient
  ) { }

  upload(formData: FormData, cycle_id: number, group_id: number, area_id: number, content_id: number) {
    return this.http.post<any>(`${environment.baseURL}/documents/upload/${cycle_id}/${group_id}/${area_id}/${content_id}`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  uploadForAssignment(formData: FormData, cycle_id: number, group_id: number, area_id: number, assignment_id: number) {
    return this.http.post<any>(`${environment.baseURL}/documents/uploadForAssignment/${cycle_id}/${group_id}/${area_id}/${assignment_id}`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  delete(id: number) {
    return this.http.delete<any>(`${environment.baseURL}/documents/delete/${id}`);
  }
}
