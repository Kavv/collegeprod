import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }
  
  base = environment.baseURL;
  getStudentsByDateRange(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getStudentsByDateRange`, body, { responseType: 'blob' });
  }
  getStudentsByCycle(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getStudentsByCycle`, body, { responseType: 'blob' });
  }
  getStudentsByGroup(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getStudentsByGroup`, body, { responseType: 'blob' });
  }

  getAssistanceByStudent(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getAssistanceByStudent`, body, { responseType: 'blob' });
  }

  getAssistanceByGroup(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getAssistanceByGroup`, body, { responseType: 'blob' });
  }
  
  getAssistanceByCycle(body:any) {
    return this.http.post(`${environment.baseURL}/reports/getAssistanceByCycle`, body, { responseType: 'blob' });
  }
}

