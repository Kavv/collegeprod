import {CanActivateFn, Router} from '@angular/router';
import {AuthService} from "../../services/auth/auth.service";
import {inject} from "@angular/core";

export const authGuard: CanActivateFn = (route, state) => {
  const authenticated = inject(AuthService).isLoggedIn();
  const router = inject(Router);

  console.log(authenticated);
  if (!authenticated) {
    router.navigate(['auth', 'login']);
    return false;
  }

  return true;
};
