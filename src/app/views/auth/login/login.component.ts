import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth/auth.service";
import {Router} from "@angular/router";
import {LoginDto} from "../../../models/auth/login-dto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {

    this.form = this.fb.group({
      user: ['', Validators.required],
      password: ['', [Validators.required, Validators.min(12)]]
    });
  }

  login() {
    const formValues = this.form.value;
    console.log('Here');
    console.log(formValues);

    if (formValues.user && formValues.password && this.form.valid) {
      const body: LoginDto = {
        user: formValues.user,
        password: formValues.password
      }

      this.authService.login(body)
        .pipe()
        .subscribe((response) => {
          console.log(response);
          this.router.navigate(['']);
        });
    }
  }

}
