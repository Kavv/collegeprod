import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AssignmentFormComponent } from '../../assignments/dialog/assignment-form/assignment-form.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'ngx-editor';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit{

  
  constructor(
    public dialogRef: MatDialogRef<AssignmentFormComponent>,
    private formBuilder: FormBuilder,
    private authServices: AuthService,
    private snackbar: MatSnackBar,
  ) {}

  passwordForm!: FormGroup;
  hide1 = true;
  hide2 = true;

  ngOnInit(): void {
    
    this.passwordForm = this.formBuilder.group({
      currentPassword: [null, [Validators.required]],
      newPassword: [null, [Validators.required]],
    });
  }


  updatePassword() {
    if (this.passwordForm.valid) {
      this.authServices.changePassword(this.passwordForm.value).subscribe({
        next: (data) => {
          this.snackbar.open('Contraseña actualizada', undefined, {
            duration: 1000
          });
          this.closeModal();
        }, 
        error: (error) => {
          console.log(error);
          this.snackbar.open(error.error.error, undefined, {
            duration: 1000
          });
        }
      });
    }
  }

  closeModal() {
    this.dialogRef.close(true);
  } 
}
