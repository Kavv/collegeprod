import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssistancesComponent } from './assistances.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssistancesRoutingModule { }
