import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable, take } from 'rxjs';
import { AssistancesDto } from 'src/app/models/assistances/assistances-dto';
import { CreateAssistanceDto } from 'src/app/models/assistances/create-assistance-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { TeacherDto } from 'src/app/models/teachers/teacher-dto';
import { AssistancesService } from 'src/app/services/assistances/assistances.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { UsersService } from 'src/app/services/users/users.service';
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation-dialog/confirmation-dialog.component';
import { MatSort } from '@angular/material/sort';

@UntilDestroy()
@Component({
  selector: 'app-assistances',
  templateUrl: './assistances.component.html',
  styleUrls: ['./assistances.component.scss']
})
export class AssistancesComponent implements OnInit, AfterViewInit {

  assistances: MatTableDataSource<AssistancesDto> = new MatTableDataSource<AssistancesDto>([]);
  dataColumns = ['date', 'student', 'status', 'teacher', 'group', 'cycle', 'actions'];
  @ViewChild(MatSort) sort!: MatSort;

  editingId!: number | undefined;
  editing!: boolean;

  teachers!: any[];
  students!: any[];
  cycles$!: Observable<CycleDto[]>;
  groups$!: GroupDto[];

  assistanceForm!: FormGroup;

  statusValues = [
    {
      value: 'PRESENTE',
      label: 'PRESENTE'
    },
    {
      value: 'AUSENTE',
      label: 'AUSENTE'
    },
    {
      value: 'JUSTIFICADO',
      label: 'JUSTIFICADO'
    }
  ]

  constructor(
    private fb: FormBuilder,
    private _cycleServices: CyclesService,
    private _userService: UsersService,
    private _assistanceService: AssistancesService,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {
   this.initForm();
  }

  ngOnInit(): void {
    this.loadInitialData();
  }

  ngAfterViewInit(): void {
    this.assistances.sort = this.sort;
  }

  initForm() {
    this.assistanceForm = this.fb.group({
      date: ['', Validators.required],
      status: ['', Validators.required],
      teacher_id: [null, Validators.required],
      student_id: [null, Validators.required],
      group_id: [null, Validators.required],
      cycle_id: [null, Validators.required]
    });

    this.assistanceForm.controls['group_id'].disable();
    this.assistanceForm.controls['teacher_id'].disable();
    this.assistanceForm.controls['student_id'].disable();

    const cycleControl = this.assistanceForm.get('cycle_id');
    const groupControl = this.assistanceForm.get('group_id');

    if (cycleControl) {
      cycleControl.valueChanges.subscribe((value) => {
        if (value) {
          this._cycleServices.getCycleStructure(value).pipe(take(1))
          .subscribe((groups) => {
            this.assistanceForm.controls['group_id'].enable();
            this.groups$ = groups;
          });
        }
       
      });
    }

    if (groupControl) {
      groupControl.valueChanges.subscribe((value) => {
        if (value) {
          this.teachers = value.teacher;
          this.students = value.students;
          this.assistanceForm.controls['teacher_id'].enable();
          this.assistanceForm.controls['student_id'].enable();
        }
        
      })
    }
  }

  loadInitialData(updateCycle = true) {
    this._assistanceService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe((assistances) => {
        this.assistances.data = assistances;
      });

    if(updateCycle) {
      this.cycles$ = this._cycleServices.getAllCycles()
      .pipe(untilDestroyed(this));
    }


    
  }

  triggerEdit(element: AssistancesDto) {
    this.editingId = element.id;
    this.editing = true;

    this.assistanceForm = this.fb.group({
      date: [element.date, Validators.required],
      status: [element.status, Validators.required],
      teacher_id: [element.teacher_id, Validators.required],
      student_id: [element.student_id, Validators.required],
      cycle_id: [element.cycle_id, Validators.required],
      group_id: [element.group_id, Validators.required]
    });

    this._cycleServices.getCycleStructure(element.cycle_id).pipe(take(1))
    .subscribe((groups) => {
      this.groups$ = groups;
      const selectedGroup = groups.find((data) => data.id === element.group_id)
      this.assistanceForm.get('group_id')?.setValue(selectedGroup);
    });
    const cycleControl = this.assistanceForm.get('cycle_id');
    const groupControl = this.assistanceForm.get('group_id');

    if (cycleControl) {
      cycleControl.valueChanges.subscribe((value) => {
        if (value) {
          this._cycleServices.getCycleStructure(value).pipe(take(1))
          .subscribe((groups) => {
            this.assistanceForm.controls['group_id'].disable();
            this.groups$ = groups;
          });
        }
       
      });
    }

    if (groupControl) {
      groupControl.valueChanges.subscribe((value) => {
        if (value) {
          this.teachers = value.teacher;
          this.students = value.students;
          this.assistanceForm.controls['teacher_id'].enable();
          this.assistanceForm.controls['student_id'].enable();
        }
      })
    }
  }

  getStatusLabel(status: string) {
    switch(status) {
      case 'assist':
        return 'PRESENTE';
      case 'non-assist':
        return 'AUSENTE';
      case 'justify':
        return 'JUSTIFICADO';
      default:
        return '-';
    }
  }

  onDelete(id: number) {
    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);

    confirmationDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this._assistanceService.delete(id)
          .subscribe((response) => {
            if (response === 1) {
              this.loadInitialData();
              this.snackbar.open('Asistencia removida con exito', 'Cerrar', {
                duration: 1000
              });
            }
          });
      }
    })
  }

  onCreate() {
    const values = this.assistanceForm.value;

    if (values.date && values.status && values.teacher_id && values.student_id && values.cycle_id && values.group_id) {
      const body: CreateAssistanceDto = {
        date: values.date,
        status: values.status,
        teacher_id: values.teacher_id,
        student_id: values.student_id,
        cycle_id: values.cycle_id,
        group_id: values.group_id.id
      };

      if (this.editing && this.editingId) {
        this._assistanceService.update(body, this.editingId)
          .subscribe((response) => {
            this.editing = false;
            this.editingId = undefined;

            this.assistanceForm.reset();
            this.loadInitialData();
            this.snackbar.open('Asistencia actualizada con exito', 'Cerrar', {
              duration: 1000
            });
          });
        return;
      }

      this._assistanceService.create(body)
        .subscribe((response) => {
          this.assistanceForm.get('student_id')?.reset();
          this.assistanceForm.get('student_id')?.markAsPristine();
          this.loadInitialData(false);
          //this.initForm();

          this.snackbar.open('Asistencia registrada con éxito', 'Cerrar', {
            duration: 1000
          });
        })
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.assistances.filter = filterValue.trim().toLowerCase();
  }


}
