import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssignmentModalDto } from 'src/app/models/assignments/assignment-modal-dto';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent {

  constructor(
    private assignmentsService: AssignmentsService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DeleteConfirmationComponent>,
  ) {}

  
  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];
  
  delete() {
    this.assignmentsService.delete(this.data.id).subscribe({
      next: (value) => {
        this.openSnackBar("¡Registro eliminado exitosamente!", "style-success");
        this.dialogRef.close();
      },
      error: (error) => {
        console.log(error);
        this.openSnackBar(this.messageError[0][error.status], "style-error");
      }
    });
  }

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  openSnackBar(msg:string, type:string) {
    this._snackBar.open(msg, 'Cerrar', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
      duration: 2000,
    });
  }
}
