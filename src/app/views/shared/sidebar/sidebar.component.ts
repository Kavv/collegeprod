import { Component, OnInit } from '@angular/core';
import { ProfileDto } from 'src/app/models/auth/profile-dto';
import { UserDto } from 'src/app/models/users/usuario-dto';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    private authService: AuthService,
  ) { }

  currentUser!: ProfileDto;
  ngOnInit(): void {
    this.authService.getCurrentUser().subscribe((user) => {
      this.authService.currentUser = user;
      this.currentUser = user;
    });
  }

}
