import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ChangePasswordComponent } from '../../auth/change-password/change-password.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(
    private readonly authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }
  
  toggleSideBar() {
    this.toggleSideBarForMe.emit();
  }

  loggout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

  changePasswordDialog() {
    this.dialog.open(ChangePasswordComponent).afterClosed().subscribe(() => {
      
    });
  }
}
