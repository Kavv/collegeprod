import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Editor } from 'ngx-editor';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { AssignmentModalDto } from 'src/app/models/assignments/assignment-modal-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AreasService } from 'src/app/services/areas/assignments.service';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { GroupsService } from 'src/app/services/groups/groups.service';

@Component({
  selector: 'app-assignment-form',
  templateUrl: './assignment-form.component.html',
  styleUrls: ['./assignment-form.component.scss']
})
export class AssignmentFormComponent implements OnInit {

  constructor(
    private cyclesService: CyclesService,
    private groupsService: GroupsService,
    private assignmentsService: AssignmentsService,
    private areasService: AreasService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: AssignmentModalDto,
    public dialogRef: MatDialogRef<AssignmentFormComponent>,
  ) {}
  
  assignmentForm!: FormGroup;
  cycleList!: CycleDto[];
  groupsByCycle!: GroupDto[];
  areaList!: AreaDto[];
  editor!: Editor;

  
  ngOnInit(): void {
    this.cycleList = [this.data.cycle];
    this.groupsByCycle = this.data.groups;
    this.areasService.getAllAreas().subscribe((data) => this.areaList = data)
    this.setForm();
    this.editor = new Editor();
  }

  setForm() {
    this.assignmentForm = this.formBuilder.group({
      cycle: [{value: this.data?.record?.cycle ?? this.data.cycle.id ?? null, disabled: true}, [Validators.required]],
      group: [this.data?.record?.group ?? null, [Validators.required]],
      area: [this.data?.record?.area ?? null, [Validators.required]],
      name: [this.data?.record?.name ?? null, [Validators.required]],
      description: [this.data?.record?.description ?? ''],
      points: [this.data?.record?.points ?? null, [Validators.required, Validators.min(1)]],
      url: [this.data?.record?.url ?? null],
      startDate: [null, [Validators.required]],
      limitTime: [null, [Validators.required]],
      createdAt: [this.data?.record?.createdAt ?? null],
    });
    // If the user is editing
    if (this.data.record) {
      this.assignmentForm.get('group')?.disable();
      this.assignmentForm.get('startDate')?.setValue(new Date(this.data.record.startDate));
      this.assignmentForm.get('limitTime')?.setValue(new Date(this.data.record.limitTime));
    }
  }

  filterCycles(value: any) {

  }

  filterGroups(value: any) {

  }
  closeModal() {
    this.dialogRef.close(true);
  } 

  submitForm() {
    //Edit
    if(this.data?.record) {
      // Create
      this.assignmentsService.update(this.data.record.id, this.assignmentForm.value).subscribe({
        next: (data) => {
          this.dialogRef.close(data);
        },
        error: (error) => {
          this.snackbar.open(error.message, 'Cerrar', { duration: 500 });
        }
      });
    } else {
      const formData = this.assignmentForm.value;
      formData.group = formData.group.map((data: GroupDto) => {
        return data.id;
      })
      // Create
      this.assignmentsService.store(formData).subscribe({
        next: (data) => {
          this.dialogRef.close(data);
        },
        error: (error) => {
          this.snackbar.open(error.message, 'Cerrar', { duration: 500 });
        }
      });
    }

  }
  
  get selectedGroups(): GroupDto[] {
    return this.assignmentForm.get('group')?.value ?? [];
  }
}
