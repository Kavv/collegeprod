import { HttpEventType, HttpResponse } from '@angular/common/http';
import { AfterViewChecked, AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileUploadComponent, FileUploadValidators } from '@iplab/ngx-file-upload';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Editor, toHTML } from 'ngx-editor';
import { Observable, catchError, map, of } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { ContentDto } from 'src/app/models/contents/content-dto';
import { CreateContentDto } from 'src/app/models/contents/create-content-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AreasService } from 'src/app/services/areas/areas.service';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { DocumentService } from 'src/app/services/documents/document.service';


@UntilDestroy()
@Component({
  selector: 'app-edit-content-dialog',
  templateUrl: './edit-content-dialog.component.html',
  styleUrls: ['./edit-content-dialog.component.scss']
})
export class EditContentDialogComponent implements OnInit, OnDestroy, AfterViewInit {

  editForm!: FormGroup;
  cycles$!: Observable<CycleDto[]>;
  groups$!: Observable<GroupDto[]>;
  areas$!: Observable<AreaDto[]>;
  editor!: Editor;
  @ViewChild('fileUpload') fileUpload!: FileUploadComponent;

  constructor(
    public dialogRef: MatDialogRef<EditContentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ContentDto,
    private fb: FormBuilder,
    private contentService: ContentsService,
    private areaService: AreasService,
    private cycleService: CyclesService,
    private documentService: DocumentService,
    private snackbar: MatSnackBar
  ) {
    this.initialize();
  }

  ngOnInit(): void {
    this.loadInitialData();
  }

  ngAfterViewInit(): void {
    if (this.fileUpload) {
      const labelElement = this.fileUpload.label;

      if (labelElement) {
        labelElement.nativeElement.children[0].innerHTML = `
          <div style="display: flex; align-items: center">
            <span class="material-icons">upload_file</span>
            <span> Arrastra o haz click aqui para subir tus documentos </span>
          </div>
        `;
      }
    }
  }

  ngOnDestroy(): void {
    this.editor.destroy();
  }

  loadInitialData() {
    this.areas$ = this.areaService.getAll()
      .pipe(untilDestroyed(this));

    this.cycles$ = this.cycleService.getAllCycles()
      .pipe(untilDestroyed(this));

    const cycleControl = this.editForm.get('cycle_id');

    if (cycleControl) {
      this.loadCycleGroups(cycleControl.value);
    }

    this.editor = new Editor();
  }


  loadCycleGroups(id: number) {
    this.groups$ = this.cycleService.getCycleStructure(id)
      .pipe(untilDestroyed(this));
  }

  uploadFiles(files: any, cycleId: number, groupId: number, areaId: number, contentId: number) {
    const values = this.editForm.value;
    files.forEach((file: any) => {
      const formData = new FormData();
      formData.append('file', file);
      file.inProgress = true;
      console.log(values.cycle_id, values.group_id, values.area_id, contentId);
      this.documentService.upload(formData, cycleId, groupId, areaId, contentId)
        .pipe(
          map((event) => {
            switch (event.type) {
              case HttpEventType.UploadProgress:
                // @ts-ignore
                file.progress = Math.round(event.loaded * 100 / event.total);
                return null;
              case HttpEventType.Response:
                return event;
              default:
                return null;
            }
          }),
          catchError((error: HttpResponse<any>) => {
            file.inProgress = false;
            return of(`${file.name} upload file.`);
          })
        ).subscribe((event: any) => {
          if (typeof ('event') === 'object') {
            console.log(event);
          }
        })
    })
  }

  initialize() {

    let description;
    try {
      description = JSON.parse(this.data.description as string);
      if (description) {
        description = toHTML(description);
      }
    } catch (e) {
      description = this.data.description;
    }
    this.editForm = this.fb.group({
      cycle_id: [this.data.cycle_id, Validators.required],
      group_id: [this.data.group_id, Validators.required],
      area_id: [this.data.area?.id, Validators.required],
      title: [this.data.title, Validators.required],
      subtitle: [this.data.subtitle],
      description: [description],
      documents: [null, FileUploadValidators.filesLimit(10)]
    });
  }

  deleteDocument(id: number) {
    this.documentService.delete(id)
      .subscribe(response => {
        if (response === 1) {
          this.snackbar.open('Documento eliminado con exito', 'Cerrar', {
            duration: 1000
          });
        }
      })
  }

  onEdit() {
    const values = this.editForm.value;

    if (values.cycle_id && values.group_id && values.area_id && values.title && values.subtitle) {
      const body: CreateContentDto = {
        cycle_id: values.cycle_id,
        group_id: values.group_id,
        area_id: values.area_id,
        title: values.title,
        subtitle: values.subtitle,
        description: values.description
      };

      this.contentService.update(body, this.data.id as number)
        .subscribe((response) => {

          if (values.documents && values.documents.length > 0) {
            this.uploadFiles(values.documents, values.cycle_id, values.group_id, values.area_id, response.id as number);
            this.dialogRef.close(response);
          } else {
            this.dialogRef.close(response);
          }
        });
    }
  }


}
