import { group } from '@angular/animations';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { FileUploadComponent, FileUploadValidators } from '@iplab/ngx-file-upload';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Editor, Validators } from 'ngx-editor';
import { Observable, catchError, map, of } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { CreateContentDto } from 'src/app/models/contents/create-content-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AreasService } from 'src/app/services/areas/areas.service';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { DocumentService } from 'src/app/services/documents/document.service';
import { GroupsService } from 'src/app/services/groups/groups.service';


@UntilDestroy()
@Component({
  selector: 'app-create-content-dialog',
  templateUrl: './create-content-dialog.component.html',
  styleUrls: ['./create-content-dialog.component.scss']
})
export class CreateContentDialogComponent implements OnInit, OnDestroy, AfterViewInit {
  cycles$!: Observable<CycleDto[]>;
  areas$!: Observable<AreaDto[]>;
  groups$!: Observable<GroupDto[]>;
  createForm!: FormGroup;
  editor!: Editor;
  @ViewChild('fileUpload') fileUpload!: FileUploadComponent;


  constructor(
    private areasService: AreasService,
    private cyclesService: CyclesService,
    private contentsService: ContentsService,
    private documentService: DocumentService,
    private dialogRef: MatDialogRef<CreateContentDialogComponent>,
    private fb: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.loadInitialData();

    const cycleControl = this.createForm.get('cycle_id');

    if (cycleControl) {
      cycleControl.valueChanges.pipe(untilDestroyed(this))
        .subscribe((response: number) => this.loadCycleGroups(response));
    }
  }

  ngAfterViewInit(): void {

    if (this.fileUpload) {
      const labelElement = this.fileUpload.label;

      if (labelElement) {
        labelElement.nativeElement.children[0].innerHTML = `
          <div style="display: flex; align-items: center">
            <span class="material-icons">upload_file</span>
            <span> Arrastra o haz click aqui para subir tus documentos </span>
          </div>
        `;
      }
    }
  }

  ngOnDestroy(): void {
    this.editor.destroy();
  }

  loadInitialData(): void {
    this.areas$ = this.areasService.getAll()
      .pipe(
        untilDestroyed(this)
      );

    this.cycles$ = this.cyclesService.getAllCycles()
        .pipe(
          untilDestroyed(this)
        );

    this.editor = new Editor();

    this.createForm = this.fb.group({
      cycle_id: [null, Validators.required],
      group_id: [null, Validators.required],
      area_id: [null, Validators.required],
      title: ['', Validators.required],
      subtitle: [''],
      description: [''],
      documents: [null, FileUploadValidators.filesLimit(10)]
    });
  }

  loadCycleGroups(id: number) {
    this.groups$ = this.cyclesService.getCycleStructure(id)
      .pipe(
        untilDestroyed(this)
      );
  }

  uploadFiles(files: any, cycleId: number, groupId: number, areaId: number, contentId: number) {
    const values = this.createForm.value;
    files.forEach((file: any) => {
      const formData = new FormData();
      formData.append('file', file);
      file.inProgress = true;
      console.log(values.cycle_id, values.group_id, values.area_id, contentId);
      this.documentService.upload(formData, cycleId, groupId, areaId, contentId)
        .pipe(
          map((event) => {
            switch (event.type) {
              case HttpEventType.UploadProgress:
                // @ts-ignore
                file.progress = Math.round(event.loaded * 100 / event.total);
                return null;
              case HttpEventType.Response:
                return event;
              default:
                return null;
            }
          }),
          catchError((error: HttpResponse<any>) => {
            file.inProgress = false;
            return of(`${file.name} upload file.`);
          })
        ).subscribe((event: any) => {
          if (typeof ('event') === 'object') {
            console.log(event);
          }
        })
    })
  }

  onCreate(): void {
    const values = this.createForm.value;

    if (values.cycle_id && values.group_id && values.area_id && values.title && values.subtitle) {

      console.log(values.description);
      const body: CreateContentDto = {
        cycle_id: values.cycle_id,
        group_id: values.group_id,
        area_id: values.area_id,
        title: values.title,
        subtitle: values.subtitle,
        description: values.description
      };

      this.contentsService.create(body)
        .subscribe((response) => {
          console.log(response);

          if (values.documents && values.documents.length > 0) {
            this.uploadFiles(values.documents, values.cycle_id, values.group_id, values.area_id, response.id as number);
            this.dialogRef.close(response);
          } else {
            this.dialogRef.close(response);
          }
        });
    }
  }

}
