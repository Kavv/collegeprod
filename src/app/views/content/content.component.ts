import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ContentDto } from 'src/app/models/contents/content-dto';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { AreasComponent } from '../areas/areas.component';
import { MatDrawer } from '@angular/material/sidenav';
import { CreateContentDialogComponent } from './create-content-dialog/create-content-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ViewDocumentsDialogComponent } from './view-documents-dialog/view-documents-dialog.component';
import { EditContentDialogComponent } from './edit-content-dialog/edit-content-dialog.component';
import { MatSort } from '@angular/material/sort';
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation-dialog/confirmation-dialog.component';
import { toHTML } from 'ngx-editor';

@UntilDestroy()
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, AfterViewInit {

  contents: MatTableDataSource<ContentDto> = new MatTableDataSource<ContentDto>([]);
  contentColumns = ['title', 'subtitle', 'description', 'area', 'actions'];
  @ViewChild('drawer') drawer!: MatDrawer;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private contentService: ContentsService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.loadInitialData();
  }

  ngAfterViewInit(): void {
    this.contents.sort = this.sort;
  }

  loadInitialData(): void {
    this.contentService.getAll()
    .pipe(untilDestroyed(this))
    .subscribe((response) => this.contents.data = response);
  }

  onCreate(): void {
    const createDialog = this.dialog.open(CreateContentDialogComponent, {
      maxWidth: '100%',
      width: '70%',
      maxHeight: '100%',
      height: '60%'
    });

    createDialog.afterClosed().subscribe((result) => {
      if (result) {
        this.loadInitialData();
      }
    });
  }

  onEdit(content: ContentDto): void {
    const editDialog = this.dialog.open(EditContentDialogComponent, {
      data: content,
      maxWidth: '100%',
      width: '70%',
      maxHeight: '100%',
      height: '60%'
    });

    editDialog.afterClosed().subscribe((result) => {
      if (result) {
        this.loadInitialData();
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contents.filter = filterValue.trim().toLowerCase();
  }

  onDelete(id: number): void {

    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);

    confirmationDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        
        this.contentService.delete(id)
          .subscribe((response) => {
            if (response === 1) {
              this.loadInitialData();
              this.snackbar.open('Registro eliminado correctamente', 'Cerrar', {
              duration: 1000
              });
            }
        });
      }
    })

  }

  onViewDocuments(content: ContentDto) {
    const documentsDialog = this.dialog.open(ViewDocumentsDialogComponent, {
      data: content.documents,
      width: '50%',
      height: '50%'
    });

    documentsDialog.afterClosed().subscribe((result) => {
      if (result) {
        this.loadInitialData();
      }
    });
  }

  openManageAreas() {
    this.dialog.open(AreasComponent);
  }

  getDescription(description: any) {
    try {
      const content = JSON.parse(description);
      if (content) {
        return toHTML(content);
      }
    } catch (e) {
      return description;
    }
    return '-';
  }
}
