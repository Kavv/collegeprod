import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DocumentService } from 'src/app/services/documents/document.service';

@Component({
  selector: 'app-view-documents-dialog',
  templateUrl: './view-documents-dialog.component.html',
  styleUrls: ['./view-documents-dialog.component.scss']
})
export class ViewDocumentsDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ViewDocumentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any[],
    private documentService: DocumentService,
    private http: HttpClient
  ) {

  }

  downloadFile(value: any) {
    const url = value.replace("C:\\xampp\\htdocs\\college\\public/", "http://127.0.0.1:8000/");
    const a = document.createElement('a');
    a.href = url;
    a.target = '_blank';
    a.click();
  }

  downloadBlobAsFile(buffer: any, fileName: string, fileType: string): void {
    console.log(buffer);
    // const data: Blob = new Blob([buffer], {
    //   type: fileType,
    // });
    // const link = document.createElement('a');
    // link.href = window.URL.createObjectURL(data);
    // link.download = fileName;
    // link.click();
  }


  deleteDoc(id: number) {
    this.documentService.delete(id)
      .subscribe((response) => {
        if (response === 1) {
          this.dialogRef.close(response);
        }
      })
  }


}
