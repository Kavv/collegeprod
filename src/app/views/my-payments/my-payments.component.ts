import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent, ColumnMode  } from '@swimlane/ngx-datatable';
import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog} from '@angular/material/dialog';
import { PaymentsService } from 'src/app/services/users/payments.service';
import { PaymentDto } from 'src/app/models/payments/payments';
import { UserPaymentDto } from 'src/app/models/payments/userPaymentsDto';

interface userDto {
  user: string;
  name: string;
  createtAt: Date;  
}

@Component({
  selector: 'app-my-payments',
  templateUrl: './my-payments.component.html',
  styleUrls: ['./my-payments.component.scss']
})
export class MyPaymentsComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public paymentsService: PaymentsService,
  ) {
  }

  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  @ViewChild(DatatableComponent) table:any = DatatableComponent;

  ColumnMode = ColumnMode;
  students:userDto[] = [];
  payments:PaymentDto[] = [];
  userPayments:UserPaymentDto[] = [];
  userPaymentsTemp:UserPaymentDto[] = [];

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];

  ngOnInit(): void {
    this.paymentsService.paymentsByUser().subscribe((data:any) => {
      this.userPayments = this.userPaymentsTemp = data;
    });
  }

  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.userPaymentsTemp.filter(function (d:any) {
      return (d.paymentName.toLowerCase().indexOf(val) !== -1) || 
      (d.amount.toLowerCase().indexOf(val) !== -1) || 
      (d.date.indexOf(val) !== -1) || 
      (d.status.toLowerCase().indexOf(val) !== -1) || 
      (d.total.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.userPayments = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

}
