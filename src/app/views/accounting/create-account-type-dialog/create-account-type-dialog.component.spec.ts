import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountTypeDialogComponent } from './create-account-type-dialog.component';

describe('CreateAccountTypeDialogComponent', () => {
  let component: CreateAccountTypeDialogComponent;
  let fixture: ComponentFixture<CreateAccountTypeDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAccountTypeDialogComponent]
    });
    fixture = TestBed.createComponent(CreateAccountTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
