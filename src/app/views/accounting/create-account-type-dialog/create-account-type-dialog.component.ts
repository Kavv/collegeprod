import {Component, OnInit} from '@angular/core';
import {AccountingService} from "../../../services/accounting/accounting.service";
import {Observable} from "rxjs";
import {AccountType} from "../../../models/accounting/account-type";
import {MatTableDataSource} from "@angular/material/table";
import {AccountsComponent} from "../accounts/accounts.component";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {Account} from "../../../models/accounting/account";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@UntilDestroy()
@Component({
  selector: 'app-create-account-type-dialog',
  templateUrl: './create-account-type-dialog.component.html',
  styleUrls: ['./create-account-type-dialog.component.scss']
})
export class CreateAccountTypeDialogComponent implements OnInit {
  accountTypes$: MatTableDataSource<AccountType> = new MatTableDataSource<AccountType>([]);
  accountTypeForm!: FormGroup;
  editingId!: number | undefined;
  editing!: boolean;
  displayedColumns = ['name', 'actions'];
  constructor(private accounting: AccountingService, private fb: FormBuilder) {
    this.accountTypeForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
   this.loadInitialData();
  }

  loadInitialData() {
    this.accounting.getAllAccountTypes()
      .pipe(untilDestroyed(this))
      .subscribe((response: AccountType[]) => {
        this.accountTypes$.data = response;
      });
  }

  onCreate() {
    const values = this.accountTypeForm.value;

    if (values.name) {
      this.accounting.createAccountType({name: values.name})
        .subscribe((response) => {
          this.accountTypeForm.reset();
          this.accountTypeForm.clearValidators();
          this.loadInitialData();
        });
    }
  }

  triggerEdit(value: AccountType) {

    this.editing = true;
    this.editingId = value.id;

    this.accountTypeForm = this.fb.group({
      name: [value.name, Validators.required]
    });

  }

  onEdit() {
    const values = this.accountTypeForm.value;

    if (values.name) {
      this.accounting.editAccountType(this.editingId as number, {name: values.name})
        .subscribe((response: AccountType) => {
          this.editingId = undefined;
          this.editing = false;
          this.loadInitialData();
          this.accountTypeForm.clearValidators();
          this.accountTypeForm.reset();
        });
    }
  }

  onDelete(id: number) {
    this.accounting.deleteAccountType(id)
      .subscribe((response) => {
        if (response === 1) {
          this.loadInitialData();
        }
      });
  }

}
