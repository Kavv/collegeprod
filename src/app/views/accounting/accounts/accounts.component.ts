import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {CreateAccountTypeDialogComponent} from "../create-account-type-dialog/create-account-type-dialog.component";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {Account} from "../../../models/accounting/account";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {AccountType} from "../../../models/accounting/account-type";
import {AccountingComponent} from "../accounting.component";
import {AccountingService} from "../../../services/accounting/accounting.service";

@UntilDestroy()
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  accounts: MatTableDataSource<Account> = new MatTableDataSource<Account>([]);
  dataColumns = ['code', 'name', 'type_id', 'actions'];
  editingId!: number | undefined;
  editing!: boolean;
  accountForm!: FormGroup;
  accountTypes$!: Observable<AccountType[]>;

  constructor(private dialog: MatDialog,
              private fb: FormBuilder,
              private accounting: AccountingService) {
    this.accountForm = this.fb.group({
      code: ['', Validators.required],
      name: ['', Validators.required],
      type_id: ['', Validators.required]
    });
  }
  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {

    this.accounting.getAllAccounts()
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        this.accounts.data = response;
      })

    this.accountTypes$ = this.accounting.getAllAccountTypes()
      .pipe(untilDestroyed(this));
  }

  onCreate() {

    const values = this.accountForm.value;

    if (values.code && values.name && values.type_id) {
      this.accounting.createAccount({
        code: values.code,
        name: values.name,
        type_id: values.type_id
      }).subscribe((response) => {
        this.loadInitialData();
        this.accountForm.reset();
      });
    }

  }

  openCreateAccountTypeDialog() {
    const createAccountTypeDialog = this.dialog.open(CreateAccountTypeDialogComponent,
      {
        width: '50%',
        height: '50%'
      });

    createAccountTypeDialog.afterClosed()
      .pipe(untilDestroyed(this))
      .subscribe((result) => {
        if (result) {
          console.log(result);
        }
      });
  }

  triggerEdit(element: Account) {
    this.editingId = element.id;
    this.editing = true;

    this.accountForm = this.fb.group({
      code: [element.code, Validators.required],
      name: [element.name, Validators.required],
      type_id: [element.type_id, Validators.required]
    });

  }

  onEdit() {
    const values = this.accountForm.value;

    if (values.code && values.name && values.type_id) {
      this.accounting.editAccount(this.editingId as number, {
        code: values.code,
        name: values.name,
        type_id: values.type_id
      }).subscribe((response) => {
        this.loadInitialData();
        this.accountForm.reset();
        this.editingId = undefined;
        this.editing = false;
      });
    }
  }

  onDelete(id: number) {
    this.accounting.deleteAccount(id)
      .subscribe((response) => {
        this.loadInitialData();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.accounts.filter = filterValue.trim().toLowerCase();
  }

}
