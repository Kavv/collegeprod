import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingComponent } from './accounting.component';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import { AccountsComponent } from './accounts/accounts.component';
import { CreateAccountTypeDialogComponent } from './create-account-type-dialog/create-account-type-dialog.component';
import {MatMenuModule} from "@angular/material/menu";
import {ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatAutocompleteModule} from "@angular/material/autocomplete";


@NgModule({
  declarations: [
    AccountingComponent,
    AccountsComponent,
    CreateAccountTypeDialogComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatAutocompleteModule,
  ],
  exports: [
    AccountingComponent
  ]
})
export class AccountingModule { }
