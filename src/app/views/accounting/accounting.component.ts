import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Movement} from "../../models/accounting/movement";
import {Observable} from "rxjs";
import {Account} from "../../models/accounting/account";
import {UserDto} from "../../models/users/usuario-dto";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AccountingService} from "../../services/accounting/accounting.service";
import {UsersService} from "../../services/users/users.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.scss']
})
export class AccountingComponent implements OnInit {
  movements: MatTableDataSource<Movement> = new MatTableDataSource<Movement>([]);
  dataColumns = ['date', 'description', 'origin_account', 'destination_account', 'amount', 'notes', 'actions'];

  editingId!: number | undefined;
  editing!: boolean;

  accounts$!: Observable<Account[]>;
  users$!: Observable<any>;

  movementForm!: FormGroup;


  constructor(private fb: FormBuilder,
              private accounting: AccountingService,
              private usersService: UsersService) {

    this.movementForm = this.fb.group({
      date: ['', Validators.required],
      description: ['', Validators.required],
      currency: ['C$', Validators.required],
      amount: ['', Validators.required],
      created_by: ['', Validators.required],
      origin_account: ['', Validators.required],
      destination_account: ['', Validators.required],
      notes: ['']
    });
  }
  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.users$ = this.usersService.all()
      .pipe(untilDestroyed(this));

    this.accounts$ = this.accounting.getAllAccounts()
      .pipe(untilDestroyed(this));

    this.accounting.getAllMovements()
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        this.movements.data = response;
      })
  }

  onCreate() {
    const values = this.movementForm.value;

    if (values.date && values.description && values.currency && values.amount && values.created_by && values.origin_account && values.destination_account) {
      const body = {
        date: values.date,
        description: values.description,
        currency: values.currency,
        amount: values.amount,
        created_by: values.created_by,
        origin_account: values.origin_account,
        destination_account: values.destination_account,
        notes: values.notes || null
      };

      this.accounting.createMovement(body)
        .subscribe((response) => {
          this.loadInitialData();
          this.movementForm.reset();
        });
    }
  }

  triggerEdit(element: Movement) {
    this.editing = true;
    this.editingId = element.id;

    this.movementForm = this.fb.group({
      date: [element.date, Validators.required],
      description: [element.description, Validators.required],
      currency: [element.currency, Validators.required],
      amount: [element.amount, Validators.required],
      created_by: [element.created_by, Validators.required],
      origin_account: [element.origin_account, Validators.required],
      destination_account: [element.destination_account, Validators.required],
      notes: [element.notes]
    });
  }

  onEdit() {
    const values = this.movementForm.value;

    if (values.date && values.description && values.currency && values.amount && values.created_by && values.origin_account && values.destination_account) {
      const body = {
        date: values.date,
        description: values.description,
        currency: values.currency,
        amount: values.amount,
        created_by: values.created_by,
        origin_account: values.origin_account,
        destination_account: values.destination_account,
        notes: values.notes || null
      };

      this.accounting.editMovement(this.editingId as number, body)
        .subscribe((response) => {
          this.loadInitialData();
          this.editingId = undefined;
          this.editing = false;
          this.movementForm.reset();
        });
    }

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.movements.filter = filterValue.trim().toLowerCase();
  }

  onDelete(id: number) {
    this.accounting.deleteMovement(id)
      .subscribe((response) => {
        if (response === 1) {
          this.loadInitialData();
        }
      });
  }

}
