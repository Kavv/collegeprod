import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CycleStructureDialogComponent } from './cycle-structure-dialog.component';

describe('CycleStructureDialogComponent', () => {
  let component: CycleStructureDialogComponent;
  let fixture: ComponentFixture<CycleStructureDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CycleStructureDialogComponent]
    });
    fixture = TestBed.createComponent(CycleStructureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
