import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CycleDto} from "../../../models/cycles/cycle-dto";
import {CyclesService} from "../../../services/cycles/cycles.service";
import {GroupDto} from "../../../models/groups/group-dto";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {Observable} from "rxjs";
@UntilDestroy()
@Component({
  selector: 'app-cycle-structure-dialog',
  templateUrl: './cycle-structure-dialog.component.html',
  styleUrls: ['./cycle-structure-dialog.component.scss']
})
export class CycleStructureDialogComponent implements OnInit {
  structure!: Observable<GroupDto[]>;

  constructor(
    private dialogRef: MatDialogRef<CycleStructureDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CycleDto,
    private cycleService: CyclesService
  ) {
  }

  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.structure = this.cycleService.getCycleStructure(this.data.id)
      .pipe(untilDestroyed(this));
  }

}
