import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon'
import { CyclesComponent } from './cycles.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ReactiveFormsModule } from '@angular/forms';



import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from "@angular/material/table";
import {MatTooltipModule} from "@angular/material/tooltip";
import { CreateCycleFormComponent } from './create-cycle-form/create-cycle-form.component';
import { EditCycleFormComponent } from './edit-cycle-form/edit-cycle-form.component';
import { CycleStructureDialogComponent } from './cycle-structure-dialog/cycle-structure-dialog.component';
import {MatListModule} from "@angular/material/list";
import { MatSortModule } from '@angular/material/sort';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};



@NgModule({
  declarations: [
    CyclesComponent,
    CreateCycleFormComponent,
    EditCycleFormComponent,
    CycleStructureDialogComponent
  ],
    imports: [
        CommonModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        NgxDatatableModule,
        MatDialogModule,
        FormsModule,
        MatSelectModule,
        MatDatepickerModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatTableModule,
        MatTooltipModule,
        MatListModule,
        MatSortModule
    ],
  exports: [
    CyclesComponent
  ],
  providers: [

  ],
})
export class CyclesModule { }
