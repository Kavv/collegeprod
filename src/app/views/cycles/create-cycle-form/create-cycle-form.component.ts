import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CreateCycleDto} from "../../../models/cycles/create-cycle-dto";
import {CyclesService} from "../../../services/cycles/cycles.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-create-cycle-form',
  templateUrl: './create-cycle-form.component.html',
  styleUrls: ['./create-cycle-form.component.scss']
})
export class CreateCycleFormComponent {

  createForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CreateCycleFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private cyclesService: CyclesService
  ) {
    this.initialize();
  }

  initialize() {
    this.createForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  create() {
    const values = this.createForm.value;

    if (values.name && values.description) {
      const body: CreateCycleDto = {
        name: values.name,
        description: values.description
      };

      this.cyclesService.createCycle(body)
        .subscribe((response) => {
          this.createForm.reset();
          this.dialogRef.close(response);
        })

    }
  }

}
