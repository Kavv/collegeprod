import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCycleFormComponent } from './create-cycle-form.component';

describe('CreateCycleFormComponent', () => {
  let component: CreateCycleFormComponent;
  let fixture: ComponentFixture<CreateCycleFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateCycleFormComponent]
    });
    fixture = TestBed.createComponent(CreateCycleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
