import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { trigger, transition, state, animate, style, AnimationEvent } from '@angular/animations';
import { DatatableComponent, ColumnMode  } from '@swimlane/ngx-datatable';
import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BehaviorSubject, Observable} from "rxjs";
import {CycleDto} from "../../models/cycles/cycle-dto";
import {CyclesService} from "../../services/cycles/cycles.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {MatTableDataSource} from "@angular/material/table";
import {CreateCycleFormComponent} from "./create-cycle-form/create-cycle-form.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {EditCycleFormComponent} from "./edit-cycle-form/edit-cycle-form.component";
import {CycleStructureDialogComponent} from "./cycle-structure-dialog/cycle-structure-dialog.component";
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation-dialog/confirmation-dialog.component';
import { MatSort } from '@angular/material/sort';

@UntilDestroy()
@Component({
  selector: 'app-cycles',
  templateUrl: './cycles.component.html',
  styleUrls: ['./cycles.component.scss']
})


export class CyclesComponent implements OnInit, OnDestroy, AfterViewInit {
  cycles: BehaviorSubject<CycleDto[]> = new BehaviorSubject<CycleDto[]>([]);
  cycles$: Observable<CycleDto[]> = this.cycles.asObservable();
  dataSource: MatTableDataSource<CycleDto> = new MatTableDataSource<CycleDto>([]);
  displayedColumns: string[] = ['name', 'description', 'actions'];
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private cyclesService: CyclesService,public dialog: MatDialog, private snackbar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.loadInitialData();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
  }

  loadInitialData() {
    this.cyclesService.getAllCycles()
      .pipe(untilDestroyed(this))
      .subscribe((response) => this.dataSource.data = response);
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(CreateCycleFormComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const cycle = result as CycleDto;
        this.snackbar.open(`El ciclo "${cycle.name}" fue creado correctamente`, 'Cerrar', {
          duration: 1000
        });
        this.loadInitialData();
      }
    })
  }

  onEditCycle(cycle: CycleDto) {
    const dialogRef = this.dialog.open(
      EditCycleFormComponent,
      {
        data: cycle
      }
    );

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedCycle = result as CycleDto;
        this.snackbar.open(`"${cycle.name}" ha sido editado correctamente.`, 'Cerrar', {
          duration: 1000
        });
        this.loadInitialData();
      }
    })
  }

  onDelete(id: number) {
    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);

    confirmationDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cyclesService.deleteCycle(id)
          .subscribe((response) => {
            if (response === 1) {
              this.loadInitialData();
              this.snackbar.open(
                'Registro eliminado correctamente',
                'Cerrar',
                {
                  duration: 1000
                }
              )
            }
          });
      }
    })
  }

  generateStructure(id: number) {
    this.cyclesService.generateCycleStructure(id)
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        console.log(response);
        this.loadInitialData();
      });
  }

  showStructure(cycle: CycleDto) {

    const dialogRef = this.dialog
      .open(CycleStructureDialogComponent, {
        data: cycle
      });

    dialogRef.afterClosed().pipe(untilDestroyed(this)).subscribe((response) => {
      console.log(response);
    });

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

