import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CycleDto} from "../../../models/cycles/cycle-dto";
import {CyclesService} from "../../../services/cycles/cycles.service";
import {CycleEditDto} from "../../../models/cycles/cycle-edit-dto";

@Component({
  selector: 'app-edit-cycle-form',
  templateUrl: './edit-cycle-form.component.html',
  styleUrls: ['./edit-cycle-form.component.scss']
})
export class EditCycleFormComponent {
  editForm!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<EditCycleFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CycleDto,
    private fb: FormBuilder,
    private cyclesService: CyclesService
  ) {
    this.initialize();
  }

  initialize() {
    this.editForm = this.fb.group({
      name: [this.data.name, Validators.required],
      description: [this.data.description, Validators.required]
    });
  }

  edit() {
    const values = this.editForm.value;

    if (values.name && values.description) {
      const body: CycleEditDto = {
        name: values.name,
        description: values.description
      }

      this.cyclesService.editCycle(this.data.id, body)
        .subscribe((response) => {
          this.dialogRef.close(response);
        });
    }
  }

}
