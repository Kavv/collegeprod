import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCycleFormComponent } from './edit-cycle-form.component';

describe('EditCycleFormComponent', () => {
  let component: EditCycleFormComponent;
  let fixture: ComponentFixture<EditCycleFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditCycleFormComponent]
    });
    fixture = TestBed.createComponent(EditCycleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
