import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable, of } from 'rxjs';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { AreasService } from 'src/app/services/areas/areas.service';
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation-dialog/confirmation-dialog.component';

@UntilDestroy()
@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {

  areas$: Observable<AreaDto[]> = of([]);
  edit: boolean = false;
  editId: number = 0;

  createForm!: FormGroup;
  editForm!: FormGroup;

  constructor(
    private areasService: AreasService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loadInitialData();
  }

  loadInitialData(): void {
    this.createForm =  this.fb.group({
      name: ['', Validators.required]
    });

    this.areas$ = this.areasService.getAll()
    .pipe(untilDestroyed(this));
  }

  onEdit(area: AreaDto) {
    this.edit = true;
    this.editId = area.id || 0;

    if (this.edit && this.editId !== 0) {
      this.editForm = this.fb.group({
        name: [area.name, Validators.required]
      });
    }
  }

  onCreate() {
    const values = this.createForm.value;

    if (values.name) {
      const body = {
        name: values.name
      }

      this.areasService.create(body)
        .subscribe((response) => {
          this.snackbar.open('Materia creada correctamente', undefined, {
            duration: 1000
          });
          this.createForm.clearValidators();
          this.createForm.reset();
          this.loadInitialData();
        })
    }
  }

  onDelete(id: number | undefined) {

    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);

    confirmationDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (id) {
          this.areasService.delete(id)
          .subscribe(response => {
            console.log(response);
            if (response === 1) {
              this.snackbar.open('Eliminado exitosamente', 'Cerrar', {
                duration: 1000
              });
              this.loadInitialData();
            }
          });
        }
      }
    });

   

  }

  onSubmitEdit(area: AreaDto) {
    const values = this.editForm.value;

    if (values.name) {
      const body = {
        name: values.name
      };

      this.areasService.update(
        body,
        area.id as number
      ).subscribe(response => {
        this.snackbar.open('Actualizado correctamente', undefined, {
          duration: 500
        });
        this.edit = false;
        this.editId = 0;
        this.editForm = new FormGroup({});
        this.loadInitialData();
      });
    }
  }

}
