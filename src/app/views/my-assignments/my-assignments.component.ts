import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { PaymentDto } from 'src/app/models/payments/payments';
import { UserPaymentDto } from 'src/app/models/payments/userPaymentsDto';
import { UserDto } from 'src/app/models/users/usuario-dto';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { SendAssignmentComponent } from './dialog/send-assignment/send-assignment.component';

@Component({
  selector: 'app-my-assignments',
  templateUrl: './my-assignments.component.html',
  styleUrls: ['./my-assignments.component.scss']
})
export class MyAssignmentsComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public assignmentsService: AssignmentsService,
  ) {
  }

  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  @ViewChild(DatatableComponent) table:any = DatatableComponent;

  ColumnMode = ColumnMode;
  students:UserDto[] = [];
  payments:PaymentDto[] = [];
  assignmentList:AssignmentDto[] = [];
  assignmentListTemp:AssignmentDto[] = [];

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];

  ngOnInit(): void {
    this.refresh();
  }

  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.assignmentListTemp.filter(function (d:AssignmentDto) {
      return (d.cycleName.toLowerCase().indexOf(val) !== -1) ||
      (d.groupName?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.name.toLowerCase().indexOf(val) !== -1) || 
      (d.description && d.description?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.points.indexOf(val) !== -1) || 
      (d.url && d.url?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.startDate.indexOf(val) !== -1) || 
      (d.limitTime.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.assignmentList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  edit(assignment: AssignmentDto) {
    /* const selectedCycle = this.filterForm?.get('cycle')?.value;
    const cycle = this.cycleList.find((cycle) => cycle.id === selectedCycle)
    const data = {
      cycle: cycle,
      groups: this.groupsByCycle,
      record: assignment,
    };
    this.dialog.open(AssignmentFormComponent, {
      data: data,
      width: '1000px',
    }).afterClosed().subscribe(() => {
      this.refresh();
    }); */
  }

  delete(assignments: AssignmentDto) {
   /*  const data = {
      title: 'Eliminar '+ assignments.name,
      id: assignments.id,
    }
    this.dialog.open(DeleteConfirmationComponent, {
      data: data,
      width: '500px',
    }).afterClosed().subscribe(() => {
      this.refresh();
    }); */
  }
  add() {
  }
  send(assignment: AssignmentDto) {
   const data = {
      assignment: assignment,
    }
    this.dialog.open(SendAssignmentComponent, {
      data: data,
      width: '1000px',
      maxHeight: '700px'
    }).afterClosed().subscribe((response) => {
      if (response) {
        this.refresh();
      }
    });
  }

  refresh() {
    this.assignmentsService.myAssignments().subscribe((data:AssignmentDto[]) => {
      this.assignmentList = this.assignmentListTemp = data;
    });
  }
}
