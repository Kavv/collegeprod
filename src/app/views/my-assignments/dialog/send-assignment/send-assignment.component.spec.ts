import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAssignmentComponent } from './send-assignment.component';

describe('SendAssignmentComponent', () => {
  let component: SendAssignmentComponent;
  let fixture: ComponentFixture<SendAssignmentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SendAssignmentComponent]
    });
    fixture = TestBed.createComponent(SendAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
