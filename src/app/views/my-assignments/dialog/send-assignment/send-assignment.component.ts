import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileUploadValidators } from '@iplab/ngx-file-upload';
import { Editor, toHTML } from 'ngx-editor';
import { catchError, map, of } from 'rxjs';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { AssignmentModalDto } from 'src/app/models/assignments/assignment-modal-dto';
import { SendAssignmentDto } from 'src/app/models/assignments/send-assignment-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { DocumentService } from 'src/app/services/documents/document.service';
import { GroupsService } from 'src/app/services/groups/groups.service';


@Component({
  selector: 'app-send-assignment',
  templateUrl: './send-assignment.component.html',
  styleUrls: ['./send-assignment.component.scss']
})
export class SendAssignmentComponent implements OnInit {

  constructor(
    private cyclesService: CyclesService,
    private groupsService: GroupsService,
    private assignmentsService: AssignmentsService,
    private documentService: DocumentService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: SendAssignmentDto,
    public dialogRef: MatDialogRef<SendAssignmentComponent>,
  ) {}
  
  resultForm!: FormGroup;
  cycleList!: CycleDto[];
  groupsByCycle!: GroupDto[];
  editor!: Editor;

  documents: any = [];
  
  ngOnInit(): void {
    if (this.data.assignment.scoreId) {
      this.assignmentsService.getStudentAssignment(this.data.assignment.scoreId).subscribe((data) => {
        this.data.result = data;
        this.resultForm.get('record')?.setValue(data.id);
        this.resultForm.get('answer')?.setValue(data.answer);
        //this.resultForm.get('documents')?.setValue(data.documents);
        this.documents = data.documents;
        this.resultForm.get('assignment')?.setValue(data.assignment);
      });
    }
    this.setForm();
  }

  setForm() {
    this.resultForm = this.formBuilder.group({
      record: [this.data?.result?.id ?? null],
      answer: [this.data?.result?.answer ?? ''],
      documents: [this.data?.result?.documents ?? null, FileUploadValidators.filesLimit(3)],
      assignment: [this.data.assignment.id, [Validators.required]],
    });
    this.editor = new Editor();
  }

  filterCycles(value: any) {

  }

  filterGroups(value: any) {

  }
  closeModal() {
    this.dialogRef.close(false);
  } 

  submitForm() {
    const formData = this.resultForm.value;
    const invalidForm = !formData.answer && !formData.documents;
    if (invalidForm) {
      this.snackbar.open('Debes subir un archivo o plantear tu respuesta', 'Cerrar', { duration: 4000 });
      return;
    }
    // Create
    this.assignmentsService.sendMyAssignment(formData).subscribe({
      next: (data) => {
        if (formData.documents && formData.documents.length > 0) {
          const assignment = this.data.assignment;
          this.uploadFiles(formData.documents, assignment.cycle, assignment.group, assignment.area ?? 1, data.id as number);
          this.dialogRef.close(data);
        } else {
          this.dialogRef.close(data);
        }
      },
      error: (error) => {
        this.snackbar.open(error.error.message ?? error.message, 'Cerrar', { duration: 2000 });
      }
    });
  }

  uploadFiles(files: any, cycleId: number, groupId: number, areaId: number, contentId: number) {
    const values = this.resultForm.value;
    files.forEach((file: any) => {
      const formData = new FormData();
      formData.append('file', file);
      file.inProgress = true;
      this.documentService.uploadForAssignment(formData, cycleId, groupId, areaId, contentId)
        .pipe(
          map((event) => {
            switch (event.type) {
              case HttpEventType.UploadProgress:
                // @ts-ignore
                file.progress = Math.round(event.loaded * 100 / event.total);
                return null;
              case HttpEventType.Response:
                return event;
              default:
                return null;
            }
          }),
          catchError((error: HttpResponse<any>) => {
            file.inProgress = false;
            return of(`${file.name} upload file.`);
          })
        ).subscribe((event: any) => {
          if (typeof ('event') === 'object') {
          }
        })
    });
  }
}
