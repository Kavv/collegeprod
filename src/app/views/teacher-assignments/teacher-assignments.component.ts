import { Component, OnInit, ViewChild } from '@angular/core';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AreasService } from 'src/app/services/areas/assignments.service';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { UsersService } from 'src/app/services/users/users.service';
import { MatDialog } from '@angular/material/dialog';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { UserDto } from 'src/app/models/users/usuario-dto';
import { PaymentDto } from 'src/app/models/payments/payments';
import { UserPaymentDto } from 'src/app/models/payments/userPaymentsDto';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { DeleteConfirmationComponent } from '../shared/dialog/delete-confirmation/delete-confirmation.component';
import { CheckAssignmentsComponent } from './dialog/check-assignments/check-assignments.component';


@Component({
  selector: 'app-teacher-assignments',
  templateUrl: './teacher-assignments.component.html',
  styleUrls: ['./teacher-assignments.component.scss']
})
export class TeacherAssignmentsComponent implements OnInit {
  
  constructor(
    public _userService: UsersService, 
    public dialog: MatDialog,
    public areasService: AreasService,
    public assignmentsService: AssignmentsService,
    public userService: UsersService,
    public cyclesService: CyclesService,
    public groupsService: GroupsService,
    private formBuilder: FormBuilder,
  ) {
  }

  areaList: AreaDto[] = [];
  areaListTemp: AreaDto[] = [];
  mainAreas: AreaDto[] = [];
  teacherGroup!: GroupDto;
  cycleList: CycleDto[] = [];
  cycleListTemp: CycleDto[] = [];
  assignmentList: AssignmentDto[] = [];
  assignmentListTemp:AssignmentDto[] = [];
  filterForm!: FormGroup;
  groupsByCycle!: GroupDto[];
  groupsByCycleTemp!: GroupDto[];
  
  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  @ViewChild(DatatableComponent) table:any = DatatableComponent;

  ColumnMode = ColumnMode;

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];
  
  ngOnInit(): void {
    this.areasService.getAllAreas().subscribe((data: AreaDto[]) => {
      this.areaList = this.areaListTemp = data;
    });

    this.userService.getTeacherAreas().subscribe((data: AreaDto[]) => {
      this.mainAreas = data;
    });
    
    this.userService.getTeacherGroup().subscribe((data: GroupDto) => {
      this.teacherGroup = data;
    });

    this.cyclesService.getAllCycles().subscribe((data: CycleDto[]) => {
      this.cycleList = this.cycleListTemp = data;
      this.filterForm.get('cycle')?.setValue(this.cycleList[0].id ?? null);
    });

    this.setFilterForm();
    this.filterForm.get('cycle')?.valueChanges.subscribe((data)=>{
      this.groupsService.getByAllByCycle(data).subscribe((data: GroupDto[]) => {
        this.groupsByCycle = this.groupsByCycleTemp = data;
      });
      this.refresh();
    });
    this.filterForm.get('area')?.valueChanges.subscribe((data)=>{
      this.refresh();
    });
    this.filterForm.get('group')?.valueChanges.subscribe((data)=>{
      this.refresh();
    });

  }

  setFilterForm() {
    this.filterForm = this.formBuilder.group({
      cycle: [null, [Validators.required]],
      area: [''],
      group: [''],
    });
  }


  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.assignmentListTemp.filter(function (d:AssignmentDto) {
      return (d.cycleName.toLowerCase().indexOf(val) !== -1) ||
      (d.groupName?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.name.toLowerCase().indexOf(val) !== -1) || 
      (d.description && d.description?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.points.indexOf(val) !== -1) || 
      (d.url && d.url?.toLowerCase()?.indexOf(val) !== -1) || 
      (d.startDate.indexOf(val) !== -1) || 
      (d.limitTime.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.assignmentList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  private refresh()
  {
    const filterFields = this.filterForm.get('cycle')?.value;
    let params = {};
    if(this.filterForm.get('cycle')?.value) params = {...params, ...{'cycle': this.filterForm.get('cycle')?.value}};
    if(this.filterForm.get('area')?.value) params = {...params, ...{'area': this.filterForm.get('area')?.value}};
    if(this.filterForm.get('group')?.value) params = {...params, ...{'group': this.filterForm.get('group')?.value}};
    

    console.log(filterFields);
    console.log(params);

    this.assignmentsService.getAllAssignments(params).subscribe((data: AssignmentDto[]) => {
      this.assignmentList = data;
      this.assignmentListTemp = data;
    });
  }

  addAssignment() {
    const selectedCycle = this.filterForm?.get('cycle')?.value;
    const cycle = this.cycleList.find((cycle) => cycle.id === selectedCycle)
    const data = {
      cycle: cycle,
      groups: this.groupsByCycle,
    }
    /* this.dialog.open(AssignmentFormComponent, {
      data: data,
      width: '1000px',
    }).afterClosed().subscribe(() => {
      this.refresh();
    }); */

  }

  edit(assignment: AssignmentDto) {
    this.dialog.open(CheckAssignmentsComponent, {
      data: assignment,
      width: '1000px',
    }).afterClosed().subscribe(() => {
      this.refresh();
    });
  }

  delete(assignments: AssignmentDto) {
    const data = {
      title: 'Eliminar '+ assignments.name,
      id: assignments.id,
    }
    this.dialog.open(DeleteConfirmationComponent, {
      data: data,
      width: '500px',
    }).afterClosed().subscribe(() => {
      this.refresh();
    });
  }
  filterGroups(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.groupsByCycleTemp.filter(function (d:GroupDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.groupsByCycle = temp;
  }
  filterCycles(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.cycleListTemp.filter(function (d:CycleDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.cycleList = temp;
  }
  filterAreas(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.areaListTemp.filter(function (d:AreaDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.areaList = temp;
  }

  resetFilter(event: any) {
    event.target.value = '';
    this.areaList = this.areaListTemp;
    this.cycleList = this.cycleListTemp;
    this.groupsByCycle = this.groupsByCycleTemp;
  }
}
