import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { AssignmentModalDto } from 'src/app/models/assignments/assignment-modal-dto';
import { AssignmentResultDto } from 'src/app/models/assignments/assignment-result';
import { SendAssignmentDto } from 'src/app/models/assignments/send-assignment-dto';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { SetAssignmentPointsComponent } from '../set-assignment-points/set-assignment-points.component';
import { toHTML } from 'ngx-editor';

@Component({
  selector: 'app-check-assignments',
  templateUrl: './check-assignments.component.html',
  styleUrls: ['./check-assignments.component.scss']
})
export class CheckAssignmentsComponent implements OnInit {

  constructor(
    private cyclesService: CyclesService,
    private groupsService: GroupsService,
    private assignmentsService: AssignmentsService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: AssignmentDto,
    public dialogRef: MatDialogRef<CheckAssignmentsComponent>,
    public dialog: MatDialog,
  ) {}
  
  resultForm!: FormGroup;
  cycleList!: CycleDto[];
  groupsByCycle!: GroupDto[];

  assignmentList: AssignmentResultDto[] = [];
  
  ngOnInit(): void {
    this.assignmentsService.getAssignmentSent(this.data.id).subscribe((data: AssignmentResultDto[]) => {
      this.assignmentList = data;
    });
      
    this.setForm();
  }

  setForm() {
    this.resultForm = this.formBuilder.group({
      points: [null,  [Validators.required]],
      comments: [null],
      id: [null,  [Validators.required]],
    });
  }

  filterCycles(value: any) {

  }

  filterGroups(value: any) {

  }
  closeModal() {
    this.dialogRef.close(false);
  } 

  submitForm() {
    const formData = this.resultForm.value;
    const invalidForm = !formData.answer && !formData.file;
    if (invalidForm) {
      this.snackbar.open('Debes subir un archivo o plantear tu respuesta', 'Cerrar', { duration: 4000 });
      return;
    }
    // Create
    this.assignmentsService.sendMyAssignment(formData).subscribe({
      next: (data) => {
        this.dialogRef.close(data);
      },
      error: (error) => {
        this.snackbar.open(error.error.message ?? error.message, 'Cerrar', { duration: 2000 });
      }
    });
  }

  setPointsModal(assignment:AssignmentResultDto) {
    
    this.dialog.open(SetAssignmentPointsComponent, {
      data: assignment,
      width: '500px',
    }).afterClosed().subscribe((result: AssignmentResultDto) => {
      if (result) {
        const foundValue = this.assignmentList.find((data)=>data.id === assignment.id);
        if (foundValue) {
          foundValue.points = result.points;
          foundValue.comments = result.comments;
        }
      }
    });
  }

  downloadFile(file: any) {
    const url = file.url.replace("C:\\xampp\\htdocs\\college\\public/", "http://127.0.0.1:8000/");
    const a = document.createElement('a');
    a.href = url;
    a.target = '_blank';
    a.click();
  }

  mathPoints(assignment: AssignmentResultDto): boolean {
    const points = assignment.points ?? 0;
    const maxPoints = assignment.maxPoints ?? 0;
    return points > (maxPoints/2);
  }
}
