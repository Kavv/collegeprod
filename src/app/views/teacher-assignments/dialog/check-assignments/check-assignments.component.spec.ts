import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckAssignmentsComponent } from './check-assignments.component';

describe('CheckAssignmentsComponent', () => {
  let component: CheckAssignmentsComponent;
  let fixture: ComponentFixture<CheckAssignmentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckAssignmentsComponent]
    });
    fixture = TestBed.createComponent(CheckAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
