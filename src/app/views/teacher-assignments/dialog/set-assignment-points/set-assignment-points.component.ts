import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { CheckAssignmentsComponent } from '../check-assignments/check-assignments.component';
import { AssignmentResultDto } from 'src/app/models/assignments/assignment-result';

@Component({
  selector: 'app-set-assignment-points',
  templateUrl: './set-assignment-points.component.html',
  styleUrls: ['./set-assignment-points.component.scss']
})
export class SetAssignmentPointsComponent implements OnInit {
  resultForm!: FormGroup;


  constructor(
    private assignmentsService: AssignmentsService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: AssignmentResultDto,
    public dialogRef: MatDialogRef<SetAssignmentPointsComponent>,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.setForm();
  }
  setForm() {
    this.resultForm = this.formBuilder.group({
      points: [this.data.points ?? null,  [Validators.required, Validators.max(this.data.maxPoints ?? 100)]],
      comments: [this.data.comments ?? null],
      scoreId: [this.data.id],
    });
  }

  setPoints() {
    if (this.resultForm.valid) {
      this.assignmentsService.setResult(this.data.id, this.resultForm.value).subscribe((data: AssignmentResultDto) => {
        this.dialogRef.close(data);
      });
    }
    console.log(this.resultForm);
  }

  closeModal() {
    this.dialogRef.close(false);
  } 
}
