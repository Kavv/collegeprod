import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetAssignmentPointsComponent } from './set-assignment-points.component';

describe('SetAssignmentPointsComponent', () => {
  let component: SetAssignmentPointsComponent;
  let fixture: ComponentFixture<SetAssignmentPointsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SetAssignmentPointsComponent]
    });
    fixture = TestBed.createComponent(SetAssignmentPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
