import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'
import { GroupsService } from 'src/app/services/groups/groups.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';

interface userDto {
  code:string;
  name:string;
  gender:string;
  birthday:string;
  phone:string;
  mother:string;
  motherPhone:string;
  father:string;
  fatherPhone:string;
}

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  host: {'class': 'main-dialog'},
})
export class EditComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<EditComponent>,
    public _userService:UsersService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private groupsService: GroupsService,
    private cyclesService: CyclesService,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      this.idStudent = data.id;
    }   
  
  userForm:any;
  idStudent:number; 
  hide = false;
  selectedValue:string = "";
  filterGroups(val:any) {}
  filterCycles(val:any) {}

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];
  cycleList: CycleDto[] = [];
  groupsByCycle!: GroupDto[];

  ngOnInit(): void {
    if(this.data)
    {
      switch (this.data.type) {
        case 1:
          this.userForm = this.formBuilder.group({
            user: [this.data.user],
            name: [this.data.name],
            password: [''],
            gender: [this.data.gender],
            birthday: [this.data.birthday],
            phone: [this.data.phone],
            mother: [this.data.mother],
            motherPhone: [this.data.mother_phone],
            father: [this.data.father],
            fatherPhone: [this.data.father_phone],
            group: [this.data.group],
            cycle: [this.data.cycle],
          });
          this.cyclesService.getAllCycles().subscribe((data: CycleDto[]) => {
            this.cycleList = data;
            this.userForm.get('cycle')?.setValue(this.data?.cycle ?? this.cycleList[0].id ?? null);
          });
          this.userForm.get('cycle')?.valueChanges.subscribe((data:any)=>{
            this.groupsService.getByAllByCycle(data).subscribe((data: GroupDto[]) => {
              this.groupsByCycle = data;
            });
          });
          break;
        case 2:
          this.userForm = this.formBuilder.group({
            user: [this.data.user],
            name: [this.data.name],
            password: [''],
            gender: [this.data.gender],
            birthday: [this.data.birthday],
            dni: [this.data.dni],
            phone: [this.data.phone],
            title: [this.data.title],
            institution: [this.data.institution],
            salary: [this.data.salary],
            address: [this.data.address]
          });
          break;
      
        case 3:
          this.userForm = this.formBuilder.group({
            user: [this.data.user],
            name: [this.data.name],
            password: [''],
            gender: [this.data.gender],
            birthday: [this.data.birthday],
            dni: [this.data.dni],
            phone: [this.data.phone],
            position: [this.data.position],
            administrative: [true],
            salary: [this.data.salary],
            address: [this.data.address],
            type: [this.data.type] //4 => staff, 3 => Administrative
          });
          this.userForm.controls.administrative.valueChanges.subscribe((val: boolean) => {
            this.userForm.controls.type.setValue(val ? 3 : 4)
          });
          break;
      
        case 4:
          this.userForm = this.formBuilder.group({
            user: [this.data.user],
            name: [this.data.name],
            password: [''],
            gender: [this.data.gender],
            birthday: [this.data.birthday],
            dni: [this.data.dni],
            phone: [this.data.phone],
            position: [this.data.position],
            administrative: [false],
            salary: [this.data.salary],
            address: [this.data.address],
            type: [this.data.type] //4 => staff, 3 => Administrative
          });
          this.userForm.controls.administrative.valueChanges.subscribe((val: boolean) => {
            this.userForm.controls.type.setValue(val ? 3 : 4)
          });
          break;
        default:
          break;
      }
    } else {
      this.userForm = this.formBuilder.group({
        code: [this.data.code],
        name: [this.data.name],
        password: [],
        gender: [''],
        birthday: [''],
        phone: [''],
        mother: [''],
        motherPhone: [''],
        father: [''],
        fatherPhone: ['']
      });
    }
  }


  
    

    updateUser(){
      this._userService.update(this.userForm.value, this.idStudent).subscribe((response:any) => {
        this.dialogRef.close(true);
        this.openSnackBar("¡Usuario editado exitosamente!", "style-success");
      },
      error => {
        this.openSnackBar(this.messageError[0][error], "style-error");
      });
    }
  
    horizontalPosition: MatSnackBarHorizontalPosition = 'start';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    openSnackBar(msg:string, type:string) {
      this._snackBar.open(msg, 'Cerrar', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: [type],
      });
    }

    restartPass(option = 1) {
      const pass = option === 1 ? "HispanoAmericano" : "Profesor-HispanoAmericano";
      this.userForm.get('password').setValue(pass);
    }

}
