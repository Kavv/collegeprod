import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormGroup, FormControl} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { UsersService } from 'src/app/services/users/users.service';

interface userDto {
  code: string;
  name: string;
  createtAt: Date;  
}

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  private idStudent:number;
  constructor(
    public dialogRef: MatDialogRef<DeleteComponent>,
    public _userService:UsersService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.idStudent = data.id;
    }

  ngOnInit(): void {
  }

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];
  deleteUser() {
    this._userService.delete(this.idStudent).subscribe((response:any) => {
      this.dialogRef.close();
      this.openSnackBar("¡Usuario eliminado exitosamente!", "style-success");
    },
    error => {
      this.openSnackBar(this.messageError[0][error], "style-error");
    });
  }

  

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  openSnackBar(msg:string, type:string) {
    this._snackBar.open(msg, 'Cerrar', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
    });
  }
}
