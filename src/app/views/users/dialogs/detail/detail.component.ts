import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';

import { UsersService } from 'src/app/services/users/users.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {


  ngOnInit(): void {
  }


  userForm:any;
  private idStudent:number;
  cycleList: CycleDto[] = [];
  groupsByCycle!: GroupDto[];
  

  constructor(
    public dialogRef: MatDialogRef<DetailComponent>,
    public _userService:UsersService,
    private formBuilder: FormBuilder,
    private groupsService: GroupsService,
    private cyclesService: CyclesService,



    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      this.idStudent = data.id;
      (data);
      if(data)
      {
        switch (data.type) {
          case 1:
            this.userForm = formBuilder.group({
              user: [{value: data.user, disabled: true}],
              name: [{value: data.name, disabled: true}],
              password: [{value: '', disabled: true}],
              gender: [{value: data.gender ?? '', disabled: true}],
              birthday: [{value: data.birthday ?? null, disabled: true}],
              phone: [{value: data.student_phone ?? '', disabled: true}],
              mother: [{value: data.mother_name ?? '', disabled: true}],
              motherPhone: [{value: data.mother_phone ?? '', disabled: true}],
              father: [{value: data.father_name ?? '', disabled: true}],
              fatherPhone: [{value: data.father_phon ?? '', disabled: true}],
              group: [{value: data.group ?? '', disabled: true}],
              cycle: [{value: data.cycle ?? '', disabled: true}],
            });
            this.cyclesService.getAllCycles().subscribe((cycles: CycleDto[]) => {
              this.cycleList = cycles;
              this.userForm.get('cycle')?.setValue(data.cycle ?? this.cycleList[0].id ?? null);
            });
            this.userForm.get('cycle')?.valueChanges.subscribe((data:any)=>{
              this.groupsService.getByAllByCycle(data).subscribe((data: GroupDto[]) => {
                this.groupsByCycle = data;
              });
            });
            break;
        
          case 2:
            this.userForm = formBuilder.group({
              user: [{value: data.user, disabled: true}],
              name: [{value: data.name, disabled: true}],
              gender: [{value: data.gender ?? '', disabled: true}],
              birthday: [{value: data.birthday ?? null, disabled: true}],
              dni: [{value: data.dni ?? '', disabled: true}],
              phone: [{value: data.phone ?? '', disabled: true}],
              title: [{value: data.title ?? '', disabled: true}],
              institution: [{value: data.institution ?? '', disabled: true}],
              salary: [{value: data.salary ?? '', disabled: true}],
              address: [{value: data.address ?? '', disabled: true}]
            });
            break;
          case 3:
            this.userForm = formBuilder.group({
              user: [{value: data.user, disabled: true}],
              name: [{value: data.name, disabled: true}],
              gender: [{value: data.gender ?? '', disabled: true}],
              birthday: [{value: data.birthday ?? null, disabled: true}],
              dni: [{value: data.dni ?? '', disabled: true}],
              phone: [{value: data.phone ?? '', disabled: true}],
              position: [{value: data.position ?? '', disabled: true}],
              administrative: true,
              salary: [{value: data.salary ?? '', disabled: true}],
              address: [{value: data.address ?? '', disabled: true}]
            });
            break;
          case 4:
            this.userForm = formBuilder.group({
              user: [{value: data.user, disabled: true}],
              name: [{value: data.name, disabled: true}],
              gender: [{value: data.gender ?? '', disabled: true}],
              birthday: [{value: data.birthday ?? null, disabled: true}],
              dni: [{value: data.dni ?? '', disabled: true}],
              phone: [{value: data.phone ?? '', disabled: true}],
              position: [{value: data.position ?? '', disabled: true}],
              administrative: false,
              salary: [{value: data.salary ?? '', disabled: true}],
              address: [{value: data.address ?? '', disabled: true}]
            });
            break;
          default:
            break;
        }
      }
    }    

    filterGroups(val:any) {}
    filterCycles(val:any) {}
    alwaysTrue() {
      this.userForm.controls.administrative.setValue(this.data.type === 3);
    }
}
