import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { AssignmentDto } from 'src/app/models/assignments/assignment-dto';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {

  cycleList: CycleDto[] = [];
  groupsByCycle!: GroupDto[];
  
  constructor(
    public dialogRef: MatDialogRef<AddComponent>,
    public _userService:UsersService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private groupsService: GroupsService,
    private cyclesService: CyclesService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {

    switch (this.data.type) {
      case 1:
        this.userForm = this.formBuilder.group({
          code: [''],
          name: [''],
          password: ['HispanoAmericano'],
          gender: [''],
          birthday: [''],
          phone: [''],
          mother: [''],
          motherPhone: [''],
          father: [''],
          fatherPhone: [''],
          type: [this.data.type],
          cycle: [this.data.type],
          group: [this.data.type]
        });
          
        this.cyclesService.getAllCycles().subscribe((data: CycleDto[]) => {
          this.cycleList = data;
          this.userForm.get('cycle')?.setValue(this.cycleList[0].id ?? null);
        });
        this.userForm.get('cycle')?.valueChanges.subscribe((data:any)=>{
          this.groupsService.getByAllByCycle(data).subscribe((data: GroupDto[]) => {
            this.groupsByCycle = data;
          });
        });
        break;
      case 2:
        this.userForm = this.formBuilder.group({
          code: [''],
          name: [''],
          password: ['Profesor-HispanoAmericano'],
          gender: [''],
          birthday: [''],
          dni: [''],
          phone: [''],
          title: [''],
          institution: [''],
          salary: [''],
          address: [''],
          type: [this.data.type]
        });
        break;
      case 3:
        this.userForm = this.formBuilder.group({
          code: [''],
          name: [''],
          password: ['Trabajador-HispanoAmericano'],
          gender: [''],
          birthday: [''],
          dni: [''],
          phone: [''],
          position: [''],
          administrative: false,
          salary: [''],
          address: [''],
          type: [4] //4 => staff, 3 => Administrative
        });
        this.userForm.controls.administrative.valueChanges.subscribe((val: boolean) => {
          this.userForm.controls.type.setValue(val ? 3 : 4)
        });
        break;
    
      default:
        break;
    }
  }
  
  filterGroups(val:any) {}
  filterCycles(val:any) {}
  userForm:any;

  
  
    hide = false;
    selectedValue:string = "";
  
    messageError: any[] = [{
      500 : "Ups... Ocurrió un error en el servidor",
      404 : "Ocurrió un error, no se encontro la página",
      403 : "Ups... No tiene suficientes permisos para realizar esta acción",
      400 : "Ha ocurrido un error, intentelo"
    }];
  
    saveUser(){
      this._userService.store(this.userForm.value).subscribe((response:any) => {
        this.dialogRef.close(true);
        this.openSnackBar("¡Usuario agregado exitosamente!", "style-success");
      },
      error => {
        this.openSnackBar(this.messageError[0][error], "style-error");
      });
    }
  
    horizontalPosition: MatSnackBarHorizontalPosition = 'start';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    openSnackBar(msg:string, type:string) {
      this._snackBar.open(msg, 'Cerrar', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: [type],
      });
    }
  
}
