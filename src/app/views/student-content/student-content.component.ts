import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { toHTML } from 'ngx-editor';
import { AreaDto } from 'src/app/models/areas/area-dto';
import { ContentDto } from 'src/app/models/contents/content-dto';
import { AreasService } from 'src/app/services/areas/areas.service';
import { AssignmentsService } from 'src/app/services/assignments/assignments.service';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-student-content',
  templateUrl: './student-content.component.html',
  styleUrls: ['./student-content.component.scss']
})
export class StudentContentComponent implements OnInit {
  
  constructor(
    public _userService: UsersService, 
    public dialog: MatDialog,
    public areasService: AreasService,
    public assignmentsService: AssignmentsService,
    public userService: UsersService,
    public cyclesService: CyclesService,
    public groupsService: GroupsService,
    private formBuilder: FormBuilder,
    private contentService: ContentsService,
  ) {
  }

  areaList: AreaDto[] = [];
  areaListTemp: AreaDto[] = [];
  contentList: ContentDto[] = [];
  contentListTemp:ContentDto[] = [];
  filterForm!: FormGroup;
  contentForm!: FormGroup;
  

  ColumnMode = ColumnMode;
  ngOnInit(): void {
    this.areasService.getAll().subscribe((data: AreaDto[]) => {
      this.areaList = this.areaListTemp = data;
    });
    
    this.filterForm = this.formBuilder.group({
      area: [null],
    });

    this.filterForm.get('area')?.valueChanges.subscribe((area) => {
      this.contentService.getStudentContentByClass(area)
      .subscribe((response) => this.contentList = this.contentListTemp = response);
    });
  }

  filterByArea(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.areaListTemp.filter(function (d:AreaDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.areaList = temp;

  }

  getDescriptionValue(value:any) {
    let description;
    try {
      description = JSON.parse(value as string);
      if (description) {
        description = toHTML(description);
      }
    } catch (e) {
      description = value;
    }
    return description;
  }
  
  downloadFile(file: any) {
    const url = file.url.replace("C:\\xampp\\htdocs\\college\\public/", "http://127.0.0.1:8000/");
    const a = document.createElement('a');
    a.href = url;
    a.target = '_blank';
    a.click();
  }

}
