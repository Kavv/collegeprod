import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CyclesComponent} from "../../cycles/cycles.component";
import {CyclesService} from "../../../services/cycles/cycles.service";
import {CycleDto} from "../../../models/cycles/cycle-dto";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {CreateGroupDto} from "../../../models/groups/create-group-dto";
import {GroupsService} from "../../../services/groups/groups.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
@UntilDestroy()
@Component({
  selector: 'app-create-group-dialog',
  templateUrl: './create-group-dialog.component.html',
  styleUrls: ['./create-group-dialog.component.scss']
})
export class CreateGroupDialogComponent implements OnInit {
  createForm: FormGroup;
  cycles!: CycleDto[];
  constructor(public dialogRef: MatDialogRef<CreateGroupDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private cyclesService: CyclesService,
              private groupsService: GroupsService) {
    this.createForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      cycle_id: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.cyclesService.getAllCycles()
      .pipe(untilDestroyed(this))
      .subscribe((response) => this.cycles = response);
  }

  createGroup() {
    const values = this.createForm.value;

    if (values.name && values.description) {
      const body: CreateGroupDto = {
        name: values.name,
        description: values.description,
        cycle_id: values.cycle_id
      };
      this.groupsService.createGroup(body)
        .subscribe((response) => {
          this.createForm.reset();
          this.dialogRef.close(response);
        });
    }
  }

}
