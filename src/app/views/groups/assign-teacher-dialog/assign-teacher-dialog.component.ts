import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UsersService} from "../../../services/users/users.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {TeachersService} from "../../../services/teachers/teachers.service";

@UntilDestroy()
@Component({
  selector: 'app-assign-teacher-dialog',
  templateUrl: './assign-teacher-dialog.component.html',
  styleUrls: ['./assign-teacher-dialog.component.scss']
})
export class AssignTeacherDialogComponent implements OnInit {

  assignForm: FormGroup;
  teachers: any[] = [];

  constructor(
    private dialogRef: MatDialogRef<AssignTeacherDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number,
    private fb: FormBuilder,
    private teacherService: TeachersService
  ) {
    this.assignForm = this.fb.group({
      group_id: [this.data],
      teacher_id: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.teacherService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe((response: any) => {
        console.log(response);
        this.teachers = response;
      });
  }

  assign() {
    const values = this.assignForm.value;

    if (values.group_id && values.teacher_id) {
      this.teacherService.assignTeacher(values)
        .subscribe((response) => {
          this.dialogRef.close(response);
        });
    }

  }

}
