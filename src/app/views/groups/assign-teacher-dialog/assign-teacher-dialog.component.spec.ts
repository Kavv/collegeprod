import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTeacherDialogComponent } from './assign-teacher-dialog.component';

describe('AssignTeacherDialogComponent', () => {
  let component: AssignTeacherDialogComponent;
  let fixture: ComponentFixture<AssignTeacherDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AssignTeacherDialogComponent]
    });
    fixture = TestBed.createComponent(AssignTeacherDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
