import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GroupsComponent} from "./groups.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatIconModule} from "@angular/material/icon";
import { CreateGroupDialogComponent } from './create-group-dialog/create-group-dialog.component';
import {MatSelectModule} from "@angular/material/select";
import { EditGroupDialogComponent } from './edit-group-dialog/edit-group-dialog.component';
import { AssignTeacherDialogComponent } from './assign-teacher-dialog/assign-teacher-dialog.component';
import { UnassignTeacherDialogComponent } from './unassign-teacher-dialog/unassign-teacher-dialog.component';
import { MatListModule } from '@angular/material/list';
import { MatSortModule } from '@angular/material/sort';



@NgModule({
  declarations: [GroupsComponent, CreateGroupDialogComponent, EditGroupDialogComponent, AssignTeacherDialogComponent, UnassignTeacherDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatTooltipModule,
    MatIconModule,
    MatSelectModule,
    MatListModule,
    MatSortModule
  ]
})
export class GroupsModule { }
