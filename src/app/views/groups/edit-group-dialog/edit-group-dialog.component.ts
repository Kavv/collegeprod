import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CyclesService} from "../../../services/cycles/cycles.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GroupDto} from "../../../models/groups/group-dto";
import {CycleDto} from "../../../models/cycles/cycle-dto";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {CreateGroupDto} from "../../../models/groups/create-group-dto";
import {GroupsService} from "../../../services/groups/groups.service";

@UntilDestroy()
@Component({
  selector: 'app-edit-group-dialog',
  templateUrl: './edit-group-dialog.component.html',
  styleUrls: ['./edit-group-dialog.component.scss']
})
export class EditGroupDialogComponent implements OnInit {
  editForm: FormGroup;
  cycles: CycleDto[] = [];

  constructor(
    private dialogRef: MatDialogRef<EditGroupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: GroupDto,
    private fb: FormBuilder,
    private cyclesService: CyclesService,
    private groupService: GroupsService
  ) {
    this.editForm = this.fb.group({
      name: [data.name, Validators.required],
      description: [data.description, Validators.required],
      cycle_id: [data.cycle_id, Validators.required]
    });
  }

  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.cyclesService.getAllCycles()
      .pipe(untilDestroyed(this))
      .subscribe((response) => this.cycles = response);
  }

  edit() {
    const values = this.editForm.value;

    if (values.name && values.description && values.cycle_id) {
      const body: CreateGroupDto = {
        name: values.name,
        description: values.description,
        cycle_id: values.cycle_id
      }

      this.groupService.editGroup(this.data.id, body)
        .subscribe((response) => {
          this.dialogRef.close(response);
        });
    }
  }

}
