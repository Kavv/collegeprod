import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {GroupDto} from "../../models/groups/group-dto";
import {GroupsService} from "../../services/groups/groups.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {MatDialog} from "@angular/material/dialog";
import {CreateGroupDialogComponent} from "./create-group-dialog/create-group-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {EditGroupDialogComponent} from "./edit-group-dialog/edit-group-dialog.component";
import {AssignTeacherDialogComponent} from "./assign-teacher-dialog/assign-teacher-dialog.component";
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation-dialog/confirmation-dialog.component';
import { UnassignTeacherDialogComponent } from './unassign-teacher-dialog/unassign-teacher-dialog.component';
import { MatSort } from '@angular/material/sort';

@UntilDestroy()
@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit, AfterViewInit {
  groupColumns: string[] = ['name', 'description', 'cycle', 'teacher', 'actions'];
  groups: MatTableDataSource<GroupDto> = new MatTableDataSource<GroupDto>([]);

  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private groupsService: GroupsService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.loadInitialData();
  }

  ngAfterViewInit(): void {
    this.groups.sort = this.sort;
  }

  loadInitialData(): void {
    this.groupsService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe((response: GroupDto[]) => this.groups.data = response);
  }

  onCreate() {
    const createDialog = this.dialog.open(CreateGroupDialogComponent);

    createDialog.afterClosed().subscribe((result) => {
      if (result) {
        const group = result as GroupDto;
        this.snackbar.open(`El grupo ${group.name} - ${group.description} fue creado correctamente`, 'Cerrar', {
          duration: 1000
        });
        this.loadInitialData();
      }
    });
  }

  onEdit(group: GroupDto) {
    const editDialog = this.dialog.open(EditGroupDialogComponent, {
      data: group
    });

    editDialog.afterClosed()
      .subscribe((result) => {
        if (result) {
          const group = result as GroupDto;
          this.snackbar.open(`El grupo ${group.name} - ${group.description} fue actualizado correctamente`, 'Cerrar', {
            duration: 1000
          });
          this.loadInitialData();
        }
      })
  }

  onAssignTeacher(id: number) {
    const assignTeacherDialog = this.dialog.open(AssignTeacherDialogComponent, {
      data: id
    });

    assignTeacherDialog.afterClosed()
      .subscribe((result: GroupDto) => {
        if (result) {
          this.snackbar.open('Se asigno el maestro correctamente', 'Cerrar', {
            duration: 500
          });
          this.loadInitialData();
        }
      });
  }

  onUnassignTeacher(group: GroupDto) {
    const unassignTeacherDialog = this.dialog.open(UnassignTeacherDialogComponent, {
      data: group
    });

    unassignTeacherDialog.afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.snackbar.open('Se desasigno el maestro correctamente', 'Cerrar', {
            duration: 1000
          });

          this.loadInitialData();
        }
      })
  }

  onDelete(id: number) {

    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);


    confirmationDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.groupsService.deleteGroup(id)
          .subscribe((response) => {
        this.snackbar.open('Registro eliminado correctamente', 'Cerrar', {
          duration: 1000
        });
        this.loadInitialData();
      })
      }
    });
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.groups.filter = filterValue.trim().toLowerCase();
  }

}
