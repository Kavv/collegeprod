import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassignTeacherDialogComponent } from './unassign-teacher-dialog.component';

describe('UnassignTeacherDialogComponent', () => {
  let component: UnassignTeacherDialogComponent;
  let fixture: ComponentFixture<UnassignTeacherDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UnassignTeacherDialogComponent]
    });
    fixture = TestBed.createComponent(UnassignTeacherDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
