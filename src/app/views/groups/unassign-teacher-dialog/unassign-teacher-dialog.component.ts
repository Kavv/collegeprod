import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { TeachersService } from 'src/app/services/teachers/teachers.service';

@Component({
  selector: 'app-unassign-teacher-dialog',
  templateUrl: './unassign-teacher-dialog.component.html',
  styleUrls: ['./unassign-teacher-dialog.component.scss']
})
export class UnassignTeacherDialogComponent {
  constructor(
    private dialogRef: MatDialogRef<UnassignTeacherDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: GroupDto,
    private teacherService: TeachersService
  ) {}

  unassign(user_id: number, group_id: number) {
    this.teacherService.unassignTeacher({
      group_id,
      teacher_id: user_id
    }).subscribe((response) => {
      if (response === 1) {
        this.dialogRef.close(true);
      }
    })
  }

}
