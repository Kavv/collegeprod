import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteStudentPaymentsComponent } from './delete-student-payments.component';

describe('DeleteStudentPaymentsComponent', () => {
  let component: DeleteStudentPaymentsComponent;
  let fixture: ComponentFixture<DeleteStudentPaymentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteStudentPaymentsComponent]
    });
    fixture = TestBed.createComponent(DeleteStudentPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
