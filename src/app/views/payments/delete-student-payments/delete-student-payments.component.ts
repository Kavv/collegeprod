import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { PaymentsService } from 'src/app/services/users/payments.service';

import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-delete-student-payments',
  templateUrl: './delete-student-payments.component.html',
  styleUrls: ['./delete-student-payments.component.scss']
})
export class DeleteStudentPaymentsComponent {

  private id:number;
  constructor(
    public dialogRef: MatDialogRef<DeleteStudentPaymentsComponent>,
    public paymentsService: PaymentsService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.id = data;
    }


  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];
  delete() {
    this.paymentsService.deleteUserPayment(this.id).subscribe((response:any) => {
      this.dialogRef.close();
      this.openSnackBar("¡Pago eliminado exitosamente!", "style-success");
    },
    error => {
      this.openSnackBar(this.messageError[0][error], "style-error");
    });
  }

  

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  openSnackBar(msg:string, type:string) {
    this._snackBar.open(msg, 'Cerrar', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
    });
  }
}
