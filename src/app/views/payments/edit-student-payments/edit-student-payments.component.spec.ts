import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStudentPaymentsComponent } from './edit-student-payments.component';

describe('EditStudentPaymentsComponent', () => {
  let component: EditStudentPaymentsComponent;
  let fixture: ComponentFixture<EditStudentPaymentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditStudentPaymentsComponent]
    });
    fixture = TestBed.createComponent(EditStudentPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
