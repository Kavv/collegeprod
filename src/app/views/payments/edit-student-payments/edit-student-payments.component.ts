import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { PaymentDto } from 'src/app/models/payments/payments';
import { PaymentsService } from 'src/app/services/users/payments.service';

@Component({
  selector: 'app-edit-student-payments',
  templateUrl: './edit-student-payments.component.html',
  styleUrls: ['./edit-student-payments.component.scss']
})
export class EditStudentPaymentsComponent {
  constructor(
    public dialogRef: MatDialogRef<EditStudentPaymentsComponent>,
    private _snackBar: MatSnackBar,
    private paymentsService: PaymentsService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  paymentForm: any;
  students: any;
  paymentList: any;
  bankFilterCtrl: FormControl<string|null> = new FormControl<string>('');

  ngOnInit(): void {
    const record = this.data.userPayment;
    this.paymentForm = this.formBuilder.group({
      date: [record.date ?? null, [Validators.required]],
      student: [record.userId ?? null, [Validators.required]],
      payment: [record.paymentId ?? null, [Validators.required]],
      amount: [record.amount ?? null, [Validators.required]],
      quantity: [record.quantity ?? null, [Validators.required]],
      discount: [record.discount ?? null],
      total: [record.total ?? null, [Validators.required]],
      search: '',
    });
    this.students = this.data.students;
    this.paymentList = this.data.payments;
    
    this.paymentForm.get('payment').valueChanges.subscribe((id:number) => {
      const payment = this.data.payments.find((data:PaymentDto) => data.id === id);
      if (payment) {
        this.paymentForm.get('amount').setValue(payment.amount);
        this.paymentForm.get('quantity').setValue(1);
      }
    });

    this.paymentForm.get('amount').valueChanges.subscribe((id:number) => {
      if(!this.paymentForm.get('payment').value) return;
      this.paymentForm.get('total').setValue(this.totalValue);
    });
    this.paymentForm.get('quantity').valueChanges.subscribe((id:number) => {
      if(!this.paymentForm.get('payment').value) return;
      this.paymentForm.get('total').setValue(this.totalValue);
    });
    this.paymentForm.get('discount').valueChanges.subscribe((id:number) => {
      if(!this.paymentForm.get('payment').value) return;
      this.paymentForm.get('total').setValue(this.totalValue);
    });
  }

  get getAmount(): FormControl {
    return this.paymentForm.get('amount');
  }

  get getQuantity(): FormControl {
    return this.paymentForm.get('quantity');
  }

  get getDiscount(): FormControl {
    return this.paymentForm.get('discount');
  }

  get getTotal(): FormControl {
    return this.paymentForm.get('total');
  }

  get totalValue() {
     const amount = this.getAmount.value ?? 0;
     const quantity = this.getQuantity.value ?? 0;
     const discount = this.getDiscount.value ?? 0;
     const total = (amount * quantity) - discount;
     return total < 0 ? 0 : total;
  }

  updateStudentPayment() {
    this.paymentsService.editUserPayments(this.paymentForm.value, this.data.userPayment.id).subscribe((data)=>{
      this.openSnackBar("¡Pago editado exitosamente!", "style-success");
      this.dialogRef.close(true);
    });
  }
  
  closeModal() {
    this.dialogRef.close(true);
  }

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  openSnackBar(msg:string, type:string) {
    this._snackBar.open(msg, 'Cerrar', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
    });
  }

  filterStudents(event: any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.data.students.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      (d.user.toLowerCase().indexOf(val) !== -1) || 
      (d.created_at.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.students = temp;
  }
  filterPayments(event: any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.data.payments.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });

    // update the rows
    this.paymentList = temp;
  }
}
