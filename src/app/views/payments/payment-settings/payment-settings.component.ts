import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { PaymentDto } from 'src/app/models/payments/payments';
import { PaymentsService } from 'src/app/services/users/payments.service';

@Component({
  selector: 'app-payment-settings',
  templateUrl: './payment-settings.component.html',
  styleUrls: ['./payment-settings.component.scss']
})
export class PaymentSettingsComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<PaymentSettingsComponent>,
    private _snackBar: MatSnackBar,
    private paymentsService: PaymentsService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  paymentForm: any;
  ColumnMode = ColumnMode;
  payments: PaymentDto[] = [];
  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  paymentSelected: number | null = null;

  ngOnInit(): void {
    this.paymentForm = this.formBuilder.group({
      name: [''],
      amount: [''],
      mandatory: [true],
    });
    this.getPayments();
  }

  saveSetting() {
    this.paymentsService.store(this.paymentForm.value).subscribe((data) => {
      this._snackBar.open('Guardado exitosamente', 'Cerrar', {
        duration: 2000
      });
      this.getPayments();
    });
  }

  getPayments() {
    this.paymentsService.payments().subscribe((data:any) => {
      this.payments = data;
    });
  }

  show(id: number) {

  }

  edit(row: any) {
    this.paymentForm.get('name').setValue(row.name);
    this.paymentForm.get('amount').setValue(row.amount);
    this.paymentForm.get('mandatory').setValue(row.mandatory);
    this.paymentSelected = row.id;
  }

  update() {
    this.paymentsService.update(this.paymentForm.value, this.paymentSelected).subscribe(() => {
      this.resetForm();
      this.getPayments();
    });
  }
  resetForm() {
    this.paymentForm.reset();
    this.paymentForm.markAsPristine();
    this.getPayments();
    this.paymentSelected = null;
  }
}
