import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { trigger, transition, state, animate, style, AnimationEvent } from '@angular/animations';
import { DatatableComponent, ColumnMode  } from '@swimlane/ngx-datatable';
import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PaymentSettingsComponent } from './payment-settings/payment-settings.component';
import { StudentPaymentsComponent } from './student-payments/student-payments.component';
import { PaymentsService } from 'src/app/services/users/payments.service';
import { PaymentDto } from 'src/app/models/payments/payments';
import { EditStudentPaymentsComponent } from './edit-student-payments/edit-student-payments.component';
import { DeleteStudentPaymentsComponent } from './delete-student-payments/delete-student-payments.component';
import { UserPaymentDto } from 'src/app/models/payments/userPaymentsDto';



interface userDto {
  user: string;
  name: string;
  createtAt: Date;  
}

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent {
  
  constructor(
    public _userService: UsersService, 
    public dialog: MatDialog,
    public paymentsService: PaymentsService,
  ) {
  }

  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  @ViewChild(DatatableComponent) table:any = DatatableComponent;

  ColumnMode = ColumnMode;
  students:userDto[] = [];
  payments:PaymentDto[] = [];
  userPayments:UserPaymentDto[] = [];
  userPaymentsTemp:UserPaymentDto[] = [];

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];

  ngOnInit(): void {
    this._userService.students(1).subscribe((users:any) => {
      this.students = users;
    });
    this.paymentsService.payments().subscribe((data:any) => {
      this.payments = data;
    });
    this.paymentsService.userPayments().subscribe((data:any) => {
      this.userPayments = this.userPaymentsTemp = data;
    });
  }

  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.userPaymentsTemp.filter(function (d:any) {
      return (d.userName.toLowerCase().indexOf(val) !== -1) ||
      (d.paymentName.toLowerCase().indexOf(val) !== -1) || 
      (d.user.toLowerCase().indexOf(val) !== -1) || 
      (d.amount.toLowerCase().indexOf(val) !== -1) || 
      (d.date.indexOf(val) !== -1) || 
      (d.createdAt.indexOf(val) !== -1) || 
      (d.total.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.userPayments = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  private refresh()
  {
    this.paymentsService.userPayments().subscribe((data:any) => {
      this.userPayments = data;
    });
  }

  paymentSettings() {
    
    this.dialog.open(PaymentSettingsComponent, {
      data: [],
      width: '1200px',
    }).afterClosed().subscribe(response => {
      this.refresh();
    });

  }

  studentPaymentModal() {
    const data = {
      students: this.students,
      payments: this.payments,
    }
    this.dialog.open(StudentPaymentsComponent, {
      data: data,
      width: '1000px',
    }).afterClosed().subscribe(response => {
      this.refresh();
    });
  }

  editStudentPayment(userPayment: UserPaymentDto) {
    const data = {
      userPayment: userPayment,
      students: this.students,
      payments: this.payments,
    }
    this.dialog.open(EditStudentPaymentsComponent, {
      data: data,
      width: '1000px',
    }).afterClosed().subscribe(response => {
      this.refresh();
    });
    
  }

  deleteStudentPayment(userPayment: UserPaymentDto) {
    this.dialog.open(DeleteStudentPaymentsComponent, {
      data: userPayment.id,
      width: '500px',
    }).afterClosed().subscribe(response => {
      this.refresh();
    });
  }
}
