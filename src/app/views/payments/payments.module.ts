import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PaymentsComponent } from './payments.component';
import { PaymentSettingsComponent } from './payment-settings/payment-settings.component';
import { StudentPaymentsComponent } from './student-payments/student-payments.component';
import { EditStudentPaymentsComponent } from './edit-student-payments/edit-student-payments.component';
import { DeleteStudentPaymentsComponent } from './delete-student-payments/delete-student-payments.component';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  declarations: [
    PaymentsComponent,
    PaymentSettingsComponent,
    StudentPaymentsComponent,
    EditStudentPaymentsComponent,
    DeleteStudentPaymentsComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    NgxDatatableModule,
    MatDialogModule,
    FormsModule,
    MatSelectModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatNativeDateModule,
    MatSlideToggleModule,
  ],
  exports: [
    PaymentsComponent,
  ],
  providers: [
  ],
})
export class PaymentsModule { }
