import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Validators } from 'ngx-editor';
import { Reports } from 'src/app/models/reports/reports';
import { ReportService } from 'src/app/services/report/report.service';

@Component({
  selector: 'app-basic-range-date-form',
  templateUrl: './basic-range-date-form.component.html',
  styleUrls: ['./basic-range-date-form.component.scss']
})
export class BasicRangeDateFormComponent implements OnInit {
  reportForm: any;

  constructor(
    public dialogRef: MatDialogRef<BasicRangeDateFormComponent>,
    public reportServices: ReportService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: Reports) {
      
    }

  ngOnInit(): void {
    switch (this.data.code) {
      case 'EPRF':
        this.reportForm = this.formBuilder.group({
          start: ['', [Validators.required]],
          end: ['', [Validators.required]],
        });
        break;
    
      default:
        this.reportForm = this.formBuilder.group({
          start: [null, [Validators.required]],
          end: [null, [Validators.required]],
        });
        break;
    }
  }

  closeModal() {
    this.dialogRef.close(true);
  }

  executeReport() {
    switch (this.data.code) {
      case 'EPRF':
         this.reportServices.getStudentsByDateRange(this.reportForm.value).subscribe({
          next: (value) => {
            const fileName = 'EstudiantesPorRangoDeFecha.pdf';
            this.downloadFile(value, fileName);
          },
          error: (error) => {
            this._snackBar.open('No hay datos a exportar', 'Cerrar', { duration: 500 });
          }
         });
        break;

        case 'EPC':
          this.reportServices.getStudentsByDateRange(this.reportForm.value).subscribe({
           next: (value) => {
             const fileName = 'EstudiantesPorRangoDeFecha.pdf';
             this.downloadFile(value, fileName);
           },
           error: (error) => {
             this._snackBar.open('No hay datos a exportar', 'Cerrar', { duration: 500 });
           }
          });
        break;

        case 'EPG':
        break;
    
      default:
        
        break;
    }
  }

  downloadFile(value: any, fileName:string) {
    const blob = new Blob([value], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  }
}
