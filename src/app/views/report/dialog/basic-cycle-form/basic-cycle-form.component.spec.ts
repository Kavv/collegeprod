import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicCycleFormComponent } from './basic-cycle-form.component';

describe('BasicCycleFormComponent', () => {
  let component: BasicCycleFormComponent;
  let fixture: ComponentFixture<BasicCycleFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicCycleFormComponent]
    });
    fixture = TestBed.createComponent(BasicCycleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
