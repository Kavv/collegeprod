import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BasicRangeDateFormComponent } from '../basic-range-date-form/basic-range-date-form.component';
import { ReportService } from 'src/app/services/report/report.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder } from '@angular/forms';
import { Reports } from 'src/app/models/reports/reports';
import { Validators } from 'ngx-editor';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';

@Component({
  selector: 'app-basic-cycle-form',
  templateUrl: './basic-cycle-form.component.html',
  styleUrls: ['./basic-cycle-form.component.scss']
})
export class BasicCycleFormComponent implements OnInit {
  reportForm: any;

  constructor(
    public dialogRef: MatDialogRef<BasicRangeDateFormComponent>,
    public reportServices: ReportService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private cyclesService: CyclesService,
    @Inject(MAT_DIALOG_DATA) public data: Reports) {
      
    }
  cycleList!: CycleDto[];
  cycleListTemp!: CycleDto[];
  ngOnInit(): void {
    this.cyclesService.getAllCycles().subscribe((data: CycleDto[]) => {
      this.cycleList = this.cycleListTemp = data;
    });
    switch (this.data.code) {
      case 'EPC':
        this.reportForm = this.formBuilder.group({
          cycle: ['', [Validators.required]],
        });
        break;
      case 'APC':
        this.reportForm = this.formBuilder.group({
          cycle: ['', [Validators.required]],
        });
        break;
    
      default:
        this.reportForm = this.formBuilder.group({
          cycle: ['', [Validators.required]],
        });
        break;
    }
  }

  filterCycles(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.cycleListTemp.filter(function (d:CycleDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.cycleList = temp;
  }

  closeModal() {
    this.dialogRef.close(true);
  }

  executeReport() {
    switch (this.data.code) {
      case 'EPC':
         this.reportServices.getStudentsByCycle(this.reportForm.value).subscribe({
          next: (value) => {
            const fileName = 'EstudiantesPorRangoDeFecha.pdf';
            this.downloadFile(value, fileName);
          },
          error: (error) => {
            this._snackBar.open('No hay datos a exportar', 'Cerrar', { duration: 500 });
          }
         });
        break;
      case 'APC':
         this.reportServices.getAssistanceByCycle(this.reportForm.value).subscribe({
          next: (value) => {
            const fileName = 'AsistenciasPorCiclo.pdf';
            this.downloadFile(value, fileName);
          },
          error: (error) => {
            this._snackBar.open('No hay datos a exportar', 'Cerrar', { duration: 500 });
          }
         });
        break;
    
      default:
        
        break;
    }
  }

  downloadFile(value: any, fileName:string) {
    const blob = new Blob([value], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  }
}
