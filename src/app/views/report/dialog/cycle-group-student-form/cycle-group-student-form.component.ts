import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BasicRangeDateFormComponent } from '../basic-range-date-form/basic-range-date-form.component';
import { ReportService } from 'src/app/services/report/report.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Reports } from 'src/app/models/reports/reports';
import { Validators } from 'ngx-editor';
import { CyclesService } from 'src/app/services/cycles/cycles.service';
import { CycleDto } from 'src/app/models/cycles/cycle-dto';
import { GroupsService } from 'src/app/services/groups/groups.service';
import { GroupDto } from 'src/app/models/groups/group-dto';
import { Observable, take } from 'rxjs';

@Component({
  selector: 'app-cycle-group-student-form',
  templateUrl: './cycle-group-student-form.component.html',
  styleUrls: ['./cycle-group-student-form.component.scss']
})
export class CycleGroupStudentFormComponent implements OnInit {
  reportForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<BasicRangeDateFormComponent>,
    public reportServices: ReportService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private cyclesService: CyclesService,
    private groupsService: GroupsService,
    @Inject(MAT_DIALOG_DATA) public data: Reports) {
      
    }
  cycleList!: CycleDto[];
  cycleListTemp!: CycleDto[];
  groupsByCycle!: GroupDto[];
  groupsByCycleTemp!: GroupDto[];

  students!: any[];
  studentsTemp!: any[];
  cycles$!: Observable<CycleDto[]>;

  assistanceForm!: FormGroup;
  ngOnInit(): void {
    this.cyclesService.getAllCycles().subscribe((data: CycleDto[]) => {
      this.cycleList = this.cycleListTemp = data;
    });

    switch (this.data.code) {
      case 'APE':
        this.initForm();
        break;
    
      default:
        this.initForm();
        break;
    }
  }
  
  resetFilter(event: any) {
    event.target.value = '';
    setTimeout(() => {
      this.cycleList = this.cycleListTemp;
      this.groupsByCycle = this.groupsByCycleTemp;
      this.students = this.studentsTemp;
    }, 1000);
  }

  filterCycles(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.cycleListTemp.filter(function (d:CycleDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.cycleList = temp;
  }

  filterGroups(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.groupsByCycleTemp.filter(function (d:GroupDto) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      !val;
    });
    this.groupsByCycle = temp;
  }

  filterStudents(event:any) {
    const val = event.target.value.toLowerCase();
    const temp = this.studentsTemp.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      (d.code.toLowerCase().indexOf(val) !== -1) || 
      !val;
    });
    this.students = temp;
  }

  closeModal() {
    this.dialogRef.close(true);
  }
  
  initForm() {
    this.reportForm = this.formBuilder.group({
      cycle: ['', Validators.required],
      group: ['', Validators.required],
      student: ['', Validators.required]
    });

    const cycleControl = this.reportForm.get('cycle');
    const groupControl = this.reportForm.get('group');

    cycleControl?.valueChanges.subscribe((value) => {
      if (value) {
        this.cyclesService.getCycleStructure(value).pipe(take(1))
        .subscribe((groups) => {
          this.groupsByCycle = this.groupsByCycleTemp = groups;
        });
      }
    });

    groupControl?.valueChanges.subscribe((value) => {
      this.students = this.studentsTemp = value ? value.students : [];
    })
  }

  executeReport() {
    switch (this.data.code) {
      case 'APE':
         this.reportServices.getAssistanceByStudent(this.reportForm.value).subscribe({
          next: (value) => {
            const fileName = 'EstudiantesPorGrupo.pdf';
            this.downloadFile(value, fileName);
          },
          error: (error) => {
            this._snackBar.open('No hay datos a exportar', 'Cerrar', { duration: 500 });
          }
         });
        break;
    
      default:
        
        break;
    }
  }

  downloadFile(value: any, fileName:string) {
    const blob = new Blob([value], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  }

}
