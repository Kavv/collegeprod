import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicGroupFormComponent } from './basic-group-form.component';

describe('BasicGroupFormComponent', () => {
  let component: BasicGroupFormComponent;
  let fixture: ComponentFixture<BasicGroupFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicGroupFormComponent]
    });
    fixture = TestBed.createComponent(BasicGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
