import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReportComponent } from './report.component';
import { BasicRangeDateFormComponent } from './dialog/basic-range-date-form/basic-range-date-form.component';
import { BasicCycleFormComponent } from './dialog/basic-cycle-form/basic-cycle-form.component';
import { BasicGroupFormComponent } from './dialog/basic-group-form/basic-group-form.component';
import { CycleGroupStudentFormComponent } from './dialog/cycle-group-student-form/cycle-group-student-form.component';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  declarations: [
    ReportComponent,
    BasicRangeDateFormComponent,
    BasicCycleFormComponent,
    BasicGroupFormComponent,
    CycleGroupStudentFormComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    NgxDatatableModule,
    MatDialogModule,
    FormsModule,
    MatSelectModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatNativeDateModule,
    MatSlideToggleModule,
  ],
  exports: [
    ReportComponent,
  ],
  providers: [
  ],
})
export class ReportModule { }
