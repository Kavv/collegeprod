import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Reports } from 'src/app/models/reports/reports';
import { ReportService } from 'src/app/services/report/report.service';
import { UsersService } from 'src/app/services/users/users.service';
import { BasicRangeDateFormComponent } from './dialog/basic-range-date-form/basic-range-date-form.component';
import { BasicCycleFormComponent } from './dialog/basic-cycle-form/basic-cycle-form.component';
import { BasicGroupFormComponent } from './dialog/basic-group-form/basic-group-form.component';
import { CycleGroupStudentFormComponent } from './dialog/cycle-group-student-form/cycle-group-student-form.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  ColumnMode = ColumnMode;
  reports:Reports[] = [
    {
      'name' : 'Estudiantes por rango de fecha',
      'code' : 'EPRF'
    },
    {
      'name' : 'Estudiantes por ciclo',
      'code' : 'EPC'
    },
    {
      'name' : 'Estudiantes por grupo',
      'code' : 'EPG'
    },
    {
      'name' : 'Profesores por rango de fecha',
      'code' : 'TEST4'
    },
    {
      'name' : 'Profesores por rango de Ciclo',
      'code' : 'TEST5'
    },
    {
      'name' : 'Profesores por grupo',
      'code' : 'TEST6'
    },
    {
      'name' : 'Asistencias por grupo',
      'code' : 'APG'
    },
    {
      'name' : 'Asistancias por estudiante',
      'code' : 'APE'
    },
    {
      'name' : 'Asistancias por ciclo',
      'code' : 'APC'
    },
  ];
  
  constructor(
    public _userService: UsersService, 
    public dialog: MatDialog,
    public paymentsService: ReportService,
  ) {
  }

  ngOnInit(): void {
    console.log(this.reports);
  }

  updateFilter(value:any){}

  openForm(value:Reports) {
    switch (value.code) {
      case 'EPRF':
        this.dialog.open(BasicRangeDateFormComponent, {
          data: value,
          width: '900px',
        });
        break;
      case 'EPC':
        this.dialog.open(BasicCycleFormComponent, {
          data: value,
          width: '900px',
        });
        break;
      case 'EPG':
        this.dialog.open(BasicGroupFormComponent, {
          data: value,
          width: '900px',
        });
        break;
      case 'APE':
        this.dialog.open(CycleGroupStudentFormComponent, {
          data: value,
          width: '900px',
        });
        break;
      case 'APG':
        this.dialog.open(BasicGroupFormComponent, {
          data: value,
          width: '900px',
        });
        break;
      case 'APC':
        this.dialog.open(BasicCycleFormComponent, {
          data: value,
          width: '900px',
        });
        break;
    
      default:
        break;
    }
    
  }
}
