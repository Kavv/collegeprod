import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { UsersModule } from './views/users/users.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CyclesModule } from './views/cycles/cycles.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from './views/shared/shared.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './views/auth/login/login.component';
import { RegisterComponent } from './views/auth/register/register.component';
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {LoaderInterceptor} from "./interceptors/loader/loader.interceptor";
import {HttpRequestInterceptor} from "./interceptors/http-request/http-request.interceptor";
import {GroupsModule} from "./views/groups/groups.module";
import { PaymentsModule } from './views/payments/payments.module';
import { MyPaymentsModule } from './views/my-payments/my-payments.module';
import { ContentComponent } from './views/content/content.component';
import { MatTableModule } from '@angular/material/table';
import { AreasComponent } from './views/areas/areas.component';
import { CreateContentDialogComponent } from './views/content/create-content-dialog/create-content-dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { NgxEditorModule } from 'ngx-editor';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { EditContentDialogComponent } from './views/content/edit-content-dialog/edit-content-dialog.component';
import { ViewDocumentsDialogComponent } from './views/content/view-documents-dialog/view-documents-dialog.component';
import { AssignmentsModule } from './views/assignments/assignments.module';
import { MyAssignmentsModule } from './views/my-assignments/my-assignments.module';
import { TeacherAssignmentsComponent } from './views/teacher-assignments/teacher-assignments.component';
import { TeacherAssignmentsModule } from './views/teacher-assignments/teacher-assignments.module';
import {AccountingModule} from "./views/accounting/accounting.module";
import { StripHtmlPipe } from './pipes/strip-html.pipe';
import { AssistancesModule } from './views/assistances/assistances.module';
import { ChangePasswordComponent } from './views/auth/change-password/change-password.component';
import { ReportModule } from './views/report/report.module';
import { StudentContentModule } from './views/student-content/student-content.module';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ContentComponent,
    AreasComponent,
    CreateContentDialogComponent,
    EditContentDialogComponent,
    ViewDocumentsDialogComponent,
    StripHtmlPipe,
    ChangePasswordComponent,
  ],
  imports: [
        CommonModule,
        HttpClientModule,
        BrowserModule,
        SharedModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatSidenavModule,
        MatButtonModule,
        MatListModule,
        MatIconModule,
        MatFormFieldModule,
        MatMenuModule,
        MatTableModule,
        MatSelectModule,
        UsersModule,
        NgxDatatableModule,
        CyclesModule,
        GroupsModule,
        MatInputModule,
        MatProgressSpinnerModule,
        PaymentsModule,
        MyPaymentsModule,
        NgxEditorModule,
        FileUploadModule,
        AssignmentsModule,
        MyAssignmentsModule,
        TeacherAssignmentsModule,
        AccountingModule,
        AssistancesModule,
        ReportModule,
        StudentContentModule,
        MatSortModule
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
