import { CycleDto } from "../cycles/cycle-dto";
import { GroupDto } from "../groups/group-dto";
import { AssignmentDto } from "./assignment-dto";

export interface AssignmentResultDto {
  id: number;  
  assignment?: number;  
  assigmentName?: string;  
  student?: number;  
  studentName?: string;  
  points?: number;  
  maxPoints?: number;  
  comments?: string;  
  explanation?: string;  
  answer?: string;  
  documents?: any;  
  createdAt: string;  
}
