import { CycleDto } from "../cycles/cycle-dto";
import { GroupDto } from "../groups/group-dto";
import { AssignmentDto } from "./assignment-dto";
import { AssignmentResultDto } from "./assignment-result";

export interface SendAssignmentDto {
  assignment: AssignmentDto;
  result?: AssignmentResultDto;
}
