import { CycleDto } from "../cycles/cycle-dto";
import { GroupDto } from "../groups/group-dto";
import { AssignmentDto } from "./assignment-dto";

export interface AssignmentModalDto {
  cycle: CycleDto;
  groups: GroupDto[];
  record: AssignmentDto;  
}
