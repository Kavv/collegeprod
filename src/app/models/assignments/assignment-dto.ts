export interface AssignmentDto {
  cycle: number;
  cycleName: string;
  group: number;
  groupName?: string;
  area?: number;
  areaName?: string;
  name: string;
  description?: string;
  points: string;
  url: string;
  startDate: string;
  limitTime: string;
  createdAt: string;
  id: number;
  scoreId: number;  
  result: number;  
}
