import { AreaDto } from "../areas/area-dto";

export interface ContentDto {
  id?: number;
  title: string;
  subtitle?: string;
  description?: string;
  documents?: any[];
  cycle_id?: number;
  group_id?: number;
  area?: AreaDto;
}
