export interface CreateContentDto {
  cycle_id: number,
  group_id: number,
  area_id: number,
  title: string;
  subtitle?: string;
  description?: string;
}
