export interface CreateAccount {
  code: string;
  name: string;
  type_id: number;
}
