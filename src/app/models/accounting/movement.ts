export interface Movement {
  id: number;
  date: Date;
  description: string;
  currency: string;
  amount: number;
  notes: string;
  type: number;
  created_by: number;
  origin_account: number;
  destination_account: number;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
