export interface CreateMovement {
  date: string;
  description: string;
  currency: string;
  amount: number;
  notes?: string;
  type?: number;
  created_by: number;
  origin_account: number;
  destination_account: number;
}
