export interface Account {
  id: number;
  code: string;
  name: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  type_id: number;
  account_type: string;
}
