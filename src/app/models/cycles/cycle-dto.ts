export interface CycleDto {
  id: number;
  name: string;
  description: string;
  created_at: string;
  structure: boolean;
  updated_at: string | null;
  deleted_at: string | null;
}
