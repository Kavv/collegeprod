export interface CreateCycleDto {
  name: string;
  description: string;
}
