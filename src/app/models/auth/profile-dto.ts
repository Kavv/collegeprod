export interface ProfileDto {
    id: number;
    name: string;
    profile?: string;
    type_id?: number;
    user: string;
}
