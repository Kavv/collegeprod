export interface NewPasswordDto {
  currentPassword: string;
  newPassword: string;
}
