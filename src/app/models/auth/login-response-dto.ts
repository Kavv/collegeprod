import { AcccessTokenDto } from "./access-token-dto";

export interface LoginResponseDto {
  access_token: AcccessTokenDto;
  token_type: string;
  expires: number;
}
