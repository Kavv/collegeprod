import { ProfileDto } from "./profile-dto";

export interface AcccessTokenDto {
  token: string;
  user: ProfileDto;
}
