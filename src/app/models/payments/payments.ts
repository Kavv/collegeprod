export class PaymentDto {
    'name': string;
    'amount': number;
    'mandatory': boolean;
    'id' ?: number;
}
