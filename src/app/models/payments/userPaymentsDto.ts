export class UserPaymentDto {
    'id': number;
    'user': string;
    'userName': string;
    'paymentName': string;
    'date': number;
    'description': string;
    'amount': number;
    'quantity' ?: number;
    'discount' ?: number;
    'total' : number;
    'createdAt' : number;
    'paymentId' : number;
    'userId' : number;
    'status' ?: number;
}
