export interface AssistancesDto {
    id: number;
    date: string;
    status: string;
    teacher: any;
    teacher_id: number;
    student: any;
    student_id: number;
    group: any;
    group_id: number;
    cycle: any;
    cycle_id: number;
    created_at: string;
    updated_at: string;
    deleted_at: string;
}
