export interface CreateAssistanceDto {
    date: Date;
    status: string;
    teacher_id: number;
    student_id: number;
    group_id: number;
    cycle_id: number;
}
