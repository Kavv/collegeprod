export interface TeacherDto {
  user: any;
  user_id: number;
  gender: string;
  birthday: string;
  dni: string;
  phone: string;
  title: string;
  institution: string;
  salary: string;
  address: string;
}

