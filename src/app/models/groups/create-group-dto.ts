export interface CreateGroupDto {
  name: string;
  description: string;
  cycle_id: number;
}
