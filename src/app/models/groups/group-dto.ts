import {CycleDto} from "../cycles/cycle-dto";

export interface GroupDto {
  id: number,
  cycle_id: number,
  name: string,
  description: string;
  created_at: string;
  updated_at: string | null;
  deleted_at: string | null;
  cycle: CycleDto | null;
  students: any;
  teacher: any;
}
