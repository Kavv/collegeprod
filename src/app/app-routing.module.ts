import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsComponent } from './views/shared/layouts/layouts.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { UsersComponent } from './views/users/users.component';
import { CyclesComponent } from './views/cycles/cycles.component';
import {LoginComponent} from "./views/auth/login/login.component";
import {RegisterComponent} from "./views/auth/register/register.component";
import {authGuard} from "./guards/auth/auth.guard";
import {GroupsComponent} from "./views/groups/groups.component";
import { PaymentsComponent } from './views/payments/payments.component';
import { MyPaymentsComponent } from './views/my-payments/my-payments.component';
import { ContentComponent } from './views/content/content.component';
import { AssignmentsComponent } from './views/assignments/assignments.component';
import { MyAssignmentsComponent } from './views/my-assignments/my-assignments.component';
import { TeacherAssignmentsComponent } from './views/teacher-assignments/teacher-assignments.component';
import {AccountingComponent} from "./views/accounting/accounting.component";
import {AccountsComponent} from "./views/accounting/accounts/accounts.component";
import { AuthGuard } from './can-activate.guard';
import { AssistancesComponent } from './views/assistances/assistances.component';
import { ReportComponent } from './views/report/report.component';
import { StudentContentComponent } from './views/student-content/student-content.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutsComponent,
    canActivate: [authGuard],
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path:'users',
        canActivate: [AuthGuard],
        component: UsersComponent
      },
      {
        path:'cycles',
        canActivate: [AuthGuard],
        component: CyclesComponent
      },
      {
        path: 'groups',
        canActivate: [AuthGuard],
        component: GroupsComponent
      },
      {
        path: 'payments',
        canActivate: [AuthGuard],
        component: PaymentsComponent
      },
      {
        path:'my-payments',
        canActivate: [AuthGuard],
        component: MyPaymentsComponent
      },
      {
        path: 'contents',
        canActivate: [AuthGuard],
        component: ContentComponent
      },
      {
        path:'assignments',
        canActivate: [AuthGuard],
        component: AssignmentsComponent
      },
      {
        path:'my-assignments',
        canActivate: [AuthGuard],
        component: MyAssignmentsComponent
      },
      {
        path:'teacher-assignments',
        canActivate: [AuthGuard],
        component: TeacherAssignmentsComponent
      },
      {
        path: 'accounting',
        canActivate: [AuthGuard],
        component: AccountingComponent
      },
      {
        path: 'accounting/accounts',
        canActivate: [AuthGuard],
        component: AccountsComponent
      },
      {
        path: 'assistances',
        canActivate: [AuthGuard],
        component: AssistancesComponent
      },
      {
        path: 'report',
        canActivate: [AuthGuard],
        component: ReportComponent
      },
      {
        path: 'class-content',
        canActivate: [AuthGuard],
        component: StudentContentComponent
      }
    ]
  },
  {
    path: 'auth',
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
